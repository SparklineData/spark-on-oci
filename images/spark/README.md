# Spark-Runtime - A docker image for getting-started with Spark
====

This repository contains everything required to build a docker
image to run Spark in a containerized environment. 

This image has:

*  Java 1.8, Spark 2.2.0 preinstalled

This image does not have:

*  Anything related to Spark source code
*  tools for development or testing of Spark source code
*  tools for deploying Spark in cloud infrastructure
*  Jupyter notebooks for playing with Spark
*  Intellij, SBT, Scala, or any IDE

# 1. Build the image:

The following command will create a image called spark-runtime:latest

```
export SPARK_DOCKER_IMAGE=spark-runtime:latest
docker build -t ${SPARK_DOCKER_IMAGE} -f Dockerfile .
```

To push the container:

```
docker push ${SPARK_DOCKER_IMAGE}
```

# 2. Run the container:

The following command will run a container named spark-runtime from the image.

```
docker run -it --net=host --name spark-runtime ${SPARK_DOCKER_IMAGE} /bin/bash
```
