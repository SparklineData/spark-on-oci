import os, tarfile, tempfile, argparse, requests
import oci


def get_compartment_id():
    r = requests.get("http://169.254.169.254/opc/v1/instance/")
    if r.status_code != 200:
        raise RuntimeError(f"Unable to get instance metadata. Response: {r}")

    result = r.json()
    compartment_id = result["compartmentId"]
    return compartment_id


def init_objectstore_client():
    signer = oci.auth.signers.InstancePrincipalsSecurityTokenSigner()
    objectstore_client = oci.object_storage.ObjectStorageClient(config={},signer=signer)
    return objectstore_client


def get_or_create_bucket(os_client, bucket_name, compartment_id):
    create_bucket_request = oci.object_storage.models.CreateBucketDetails()
    create_bucket_request.compartment_id = compartment_id
    create_bucket_request.name = bucket_name
    namespace = os_client.get_namespace().data

    try:
        r = os_client.create_bucket(namespace_name=namespace, create_bucket_details=create_bucket_request).data
    except oci.exceptions.ServiceError as e:
        if e.code == "BucketAlreadyExists":
            return os_client.get_bucket(namespace_name=namespace, bucket_name=bucket_name).data
        else:
            raise (e)
    else:
        return r


def upload_directory_as_tarfile(os_client
                                , bucket_name
                                , directory
                                , object_name=None):
    """Upload a tar'd directory as 'directory.tar.gz' to bucket"""

    def tardir(path, tar_name):
        with tarfile.open(tar_name, "w:gz") as tar_handle:
            for root, dirs, files in os.walk(path):
                for file in files:
                    tar_handle.add(os.path.join(root, file))

    namespace = os_client.get_namespace().data

    with tempfile.NamedTemporaryFile() as t:
        tardir(directory, t.name)

        if not object_name:
            dirname = os.path.basename(os.path.normpath(directory))
            object_name = dirname + os.path.extsep + 'tar' + os.path.extsep + 'gz'

        result = os_client.put_object(namespace, bucket_name, object_name, t).data

    return result


def download_tarfile_as_directory(os_client
                                  , bucket_name
                                  , object_name
                                  , directory):
    namespace = os_client.get_namespace().data

    # Retrieve the file, streaming it into another file in 1 MiB chunks
    print('Retrieving file from object storage')
    get_obj = os_client.get_object(namespace, bucket_name, object_name)
    with tempfile.NamedTemporaryFile() as f:
        for chunk in get_obj.data.raw.stream(1024 * 1024, decode_content=False):
            f.write(chunk)
        with tarfile.open(f.name) as tar:
            tar.extractall(path=directory)

if __name__ == "__main__":

    description = "\n".join(["Various utilities for running OCI commands on OCI instances",
                             "Command line supports uploading and downloading of directories to OCI Object Storage",
                             "Bucket will be created is it does not exists"
                            ])

    parser = argparse.ArgumentParser(description=description,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument(dest="command",
                        help="choose 'upload' or 'download'")

    parser.add_argument(dest='bucket',
                        help="Name of object storage bucket")

    parser.add_argument(dest="object",
                        help="Name of the object to upload or download")

    parser.add_argument(dest='directory',
                        help="Path to local directory to upload or download to")

    parser.add_argument(dest='config',
                        help="Path to config file or 'PRINCIPLE' to use service principal")

    args = parser.parse_args()


    _bucket = args.bucket
    _directory = args.directory
    _command = args.command
    _object = args.object
    _config = args.config


    allowed_commands = ["upload", "download"]
    if _command not in allowed_commands:
        raise RuntimeError(f"Command '{_command}' is not one of '{allowed_commands}")

    if not os.path.isdir(_directory):
        raise RuntimeError(f"Directory '{_directory} not found")

    if _config == "PRINCIPLE":
        object_store_client = init_objectstore_client()
        compartment_id = get_compartment_id()
    elif os.path.exists(_config):
        config = oci.config.from_file(_config)
        object_store_client = oci.object_storage.ObjectStorageClient(config)
        compartment_id = config['tenancy']
    else:
        raise RuntimeError(f"Config {_config} does not match PRINCIPLE or is not a filepath")

    if _command == "upload":
        print(f"Uploading directory {_directory} to bucket {_bucket} as object {_object}")

        get_or_create_bucket(object_store_client, _bucket, compartment_id)
        upload_directory_as_tarfile(object_store_client, _bucket, _directory, _object)

    elif _command == "download":
        print(f"Downloading object {_object} from bucket {_bucket} to directory {_directory}")

        download_tarfile_as_directory(object_store_client, _bucket, _object, _directory)

    else:
        raise RuntimeError(f"Command {_command} not supported")
