#!/bin/bash
# install jupyter drive. enable with 'python -m jupyterdrive --user --mixed'
pip install git+https://github.com/jupyter/jupyter-drive.git

# install elm
# pip install elm_magic
# elm_magic install --target /opt/conda

# install nice jupyter theme.  enable with:
#  jupyter nbextension enable AdrasteaNotebookExtension/main --sys-prefix                         
pushd /tmp
git clone https://github.com/red8012/Adrastea.git
pushd Adrastea
jupyter nbextension install AdrasteaNotebookExtension --sys-prefix
popd
rm -rf Adrastea
popd

