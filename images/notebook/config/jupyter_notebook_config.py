from s3contents import S3ContentsManager
import os

#######################
# S3 CONTENTS MANAGER
#######################

s3contents_env = {
    'notebook_bucket' : "JPY_USER_BUCKET"
    ,'access_key_id' : "JPY_USER_BUCKET_ACCESS_KEY_ID"
    ,'secret_access_key': "JPY_USER_BUCKET_SECRET_ACCESS_KEY"
    ,'endpoint_url' : "JPY_USER_BUCKET_ENDPOINT_URL"
}

notebook_bucket = os.environ.get(s3contents_env['notebook_bucket'])

found = False

if notebook_bucket:

    found = True
    for key in s3contents_env.keys():
        s3contents_env[key] = os.environ.get(s3contents_env[key])
        if not s3contents_env[key]:
            env_name = s3contents_env[key]
            print(f"Please set {env_name}.")
            found = False
            break


if found:

    # Tell Jupyter to use S3ContentsManager for all storage.
    c = get_config()

    c.NotebookApp.contents_manager_class = S3ContentsManager
    c.S3ContentsManager.bucket = s3contents_env['notebook_bucket'] # "<bucket-name>>"
    c.S3ContentsManager.endpoint_url = s3contents_env['endpoint_url']
    c.S3ContentsManager.access_key_id = s3contents_env['access_key_id'] #<AWS Access Key ID / IAM Access Key ID>
    c.S3ContentsManager.secret_access_key = s3contents_env['secret_access_key'] #<AWS Secret Access Key / IAM Secret Access Key>
