#!/bin/bash

# install jupyter-scala that uses ammonite scala shell
#cd /tmp && git clone https://github.com/alexarchambault/jupyter-scala.git \
#    && cd jupyter-scala && ./jupyter-scala \
#    && cd - \
#&& cd -

# install toree 
#pip install https://dist.apache.org/repos/dist/dev/incubator/toree/0.2.0/snapshots/dev1/toree-pip/toree-0.2.0.dev1.tar.gz
#jupyter toree install --sys-prefix

# install the spylon kernel
#conda install -c conda-forge spylon-kernel --yes
#python -m spylon_kernel install --sys-prefix
