#!/bin/bash

pip install --upgrade urllib3 requests oci kubernetes git+https://gitlab.com/ali1rathore/pyhelm.git
pushd /tmp/
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod a+x kubectl
mv kubectl /opt/conda/bin
popd
