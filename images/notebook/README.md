# Spark-Notebook - A docker image for getting-started with Spark and Jupyter
====

This repository contains everything required to build a docker
image to connect to Spark with Jupyter in a containerized environment. 

This image has:

*  Bunch of other goodies that we wont support :)
  *  [PyHive](https://github.com/dropbox/PyHive) HiveThriftserver2 client for accessing Spark via JDBC in Python
  *  [Superset](https://github.com/apache/incubator-superset) for visualizing connecting to Spark
  *  [JupyterLab](https://github.com/jupyterlab/jupyterlab) for playing around only
  *  [JupyterKernelGateway](https://github.com/jupyter/kernel_gateway) for turning notebook cells into API endpoints
  *  Conda, Pandas, Cython, Bokeh, matplotlib, scipy, scikit, Seaborn, sympy, sqlalchemy and all that jazz
  *  Jupyter Notebook Server with various extensions including Scala and Spark kernels.
  *  Ready to use with [JupyterHub](https://github.com/jupyterhub/jupyterhub) and Kubernetes

This image does not have:

*  Spark preinstalled
*  Anything related to Spark source code
*  tools for development or testing of Spark source code
*  tools for deploying Spark in cloud infrastructure
*  Jupyter notebooks for playing with Spark
*  Intellij, SBT, Scala, or any IDE

# 1. Build the image:

The following command will create a image called spark-runtime:latest

```
export NOTEBOOK_IMAGE=spark-runtime:latest
docker build -t ${NOTEBOOK_IMAGE} -f Dockerfile .
```

To push the container:

```
docker push ${NOTEBOOK_IMAGE}
```

# 2. Run the container:

The following command will run a container named spark-runtime from the image.

```
docker run --rm -e EXTRA_NOTEBOOKS=/notebooks -v /path/to/your/notebooks/:/notebooks -it --net=host --name spark-runtime ${NOTEBOOK_IMAGE} /bin/bash
```

You can start the Jupyter Notebook server with start-notebook command (within the docker container).

```
spark-runtime@your-computer:~$ start-notebook
```
