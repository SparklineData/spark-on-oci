#!/bin/bash
# Install the customer jupyter-spark
pip install https://github.com/ali--/jupyter-spark/archive/master.zip

jupyter serverextension enable --py jupyter_spark --sys-prefix
jupyter nbextension install --py jupyter_spark --sys-prefix
jupyter nbextension enable --py jupyter_spark --sys-prefix
jupyter nbextension enable --py widgetsnbextension --sys-prefix
