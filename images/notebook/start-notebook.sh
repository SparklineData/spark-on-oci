#!/bin/bash

# The default url of the notebook
DEFAULT_URL=${DEFAULT_URL:-/tree}

# This is where the user's notebook server will spawn
NOTEBOOK_DIR=${NOTEBOOK_DIR:-/home/${NB_USER}}

# This directory will be copied to the user's notebook directory
EXTRA_NOTEBOOKS=${EXTRA_NOTEBOOKS:-}

# This directory's contents will be copied to user's customer css directory
THEMES_DIR=${THEMES_DIR:-}

# This file at ${THEMES_DIR}/${THEME}.custom.css wll be used for the notebooks' THEME
THEME=${THEME:-} 

# The notebook user's home directory may be overwritten during mounting.
# Set the NOTEBOOK_HOME env var to a directory who's contents will be copied into the home start notebook start
NOTEBOOK_HOME="/tmp/notebook_home"
if [[ ! -z "${NOTEBOOK_HOME}" ]] && [[ -d "${NOTEBOOK_HOME}" ]]; then
    echo Copying contents of notebook home from "${NOTEBOOK_HOME}" to "/home/${NB_USER}"
    mkdir -p ~/.jupyter ~/.ipython
    cp -r ${NOTEBOOK_HOME}/.jupyter/* ~/.jupyter/
    cp -r ${NOTEBOOK_HOME}/.ipython/* ~/.ipython/
  
fi

# Copy any extra notebooks (mounted to the /tmp/example-notebooks dir) 
#   into user's notebook dir.  This allows for notebooks to be mounted into the container
if [[ ! -z "${EXTRA_NOTEBOOKS}" ]] && [[ -d "${EXTRA_NOTEBOOKS}" ]]; then
    echo "Copying extra notebooks at ${EXTRA_NOTEBOOKS}"
    cp -nr ${EXTRA_NOTEBOOKS} ${NOTEBOOK_DIR}
fi

if [[ ! -z "${USE_SUPERSET}" ]]; then
  echo "Starting Superset"
  nohup superset runserver -d > /tmp/superset.log &
fi 

if [[ ! -z "${USE_JUPTERDRIVE}" ]]; then
  echo "starting jupyter drive plugin"
  python -m jupyterdrive --user --mixed
fi

if [[ ! -z "${NICE}" ]]; then
  echo "Enabling nice notebook theme"
  jupyter nbextension enable AdrasteaNotebookExtension/main --sys-prefix
fi

# Handle theming if the notebook
# If THEMES_DIR is set to a directory containing ${THEME}.custom.css
#   the css file will be used for the notebooks theme
if [[ ! -z "${THEMES_DIR}" ]] && [[ -d ${THEMES_DIR} ]]; then

  # create user's jupyter custom directory
  if [[ ! -d "/home/${NB_USER}/.jupyter" ]]; then
      mkdir /home/${NB_USER}/.jupyter
  fi

  echo "Copying from ${THEMES_DIR} to ~/.jupyter/custom"
  custom_dir=/home/${NB_USER}/.jupyter/custom

  cp -r ${THEMES_DIR} ${custom_dir}

  # set the this if THEME is set and ${THEME}.custom.css exists
  if [[ -f "${custom_dir}/${THEME}.custom.css" ]] ; then
    # set dark or light css theme
    echo "Setting theme to ${THEME}"
    cp -r ${custom_dir}/${THEME}.custom.css ${custom_dir}/custom.css
  fi

  # Copy custom favicon
  if [[ -f "${custom_dir}/logo.png" ]]; then
    echo "Setting custom logo from ${custom_dir}"
    # TODO Better way to do this?
    cp ${custom_dir}/logo.png /opt/conda/lib/python3.5/site-packages/notebook/static/base/images/favicon.ico
  fi

  # Add custom ipython profile configuration
  if [[ -f "${custom_dir}/profile_default" ]]; then
    echo "Creating custom ~/.ipython profile"
    mkdir -p /home/${NB_USER}/.ipython
    cp -r ${custom_dir}/profile_default /home/$NB_USER/.ipython/
  fi
fi

common_options="--notebook-dir ${NOTEBOOK_DIR} \
                --ip=${NB_IP:=0.0.0.0} --port=${NB_PORT:=8888} \
                --NotebookApp.default_url=${DEFAULT_URL:=/tree} \
                --no-browser \
                --NotebookApp.base_url=${BASE_URL:=/}"

echo "--- starting jupyter ---"

if [[ ! -z "${JUPYTERHUB_API_TOKEN}" ]]; then
  echo "Starting Singleuser notebook"
  cmd="jupyterhub-singleuser ${common_options} \
    --user="$JPY_USER" \
    --cookie-name=$JPY_COOKIE_NAME \
    --base-url=$JPY_BASE_URL \
    --hub-prefix=$JPY_HUB_PREFIX \
    --hub-api-url=$JPY_HUB_API_URL"
elif [[ ! -z "${USE_JUPYTERLAB}" ]]; then
  echo "Starting Jupyter Lab"
  cmd="jupyter lab ${common_options}"
else
  echo "Starting Jupyter Notebook"
  cmd="jupyter notebook ${common_options}"
fi

echo "${cmd}"
echo "------------------------"
${cmd}
echo "-------- done ----------"
