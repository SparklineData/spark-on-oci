#!/bin/bash
pip install superset
LAST_NAME="PutLastName"
FIRST_NAME="PutFirstName"
SUPERSET_SECRET="secret"
EMAIL="me@here.com"
fabmanager create-admin --app superset --username ${USER} --lastname ${LAST_NAME} --firstname ${FIRST_NAME} --email ${EMAIL} --password ${SUPERSET_SECRET}
superset db upgrade
superset init
