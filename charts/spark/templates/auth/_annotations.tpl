{{- define "auth-annotations" -}}
{{- if or (eq .Values.auth.type "basic") (eq .Values.auth.type "forward") -}}
ingress.kubernetes.io/auth-type: {{ .Values.auth.type -}}
{{- end -}}
{{ if (eq .Values.auth.type "forward") }}
ingress.kubernetes.io/auth-url: http://{{ .Values.auth.name }}.{{ .Release.Namespace }}.svc.cluster.local
{{ else if (eq .Values.auth.type "basic")  }}
ingress.kubernetes.io/auth-secret: {{ .Values.auth.name }}
{{ end }}
{{- end -}}
