"""An example of using kubetest to manage a deployment."""

import os
from spark_on_oci.spark_cluster import CustomResourceDefinition, SparkCluster, SparkOperator
from spark_on_oci.tools import get_package_dir
import logging

log = logging.getLogger('test_kubetool')

CLEANUP=True
NAMESPACE = "spark-test"


def get_yaml_path(file_name):
    return os.path.join(get_package_dir(), "deploy", file_name)


def _test_role(kube):

    f = get_yaml_path("role.yaml")
    role = kube.load_role(f)

    if role.is_ready():
        status = role.delete()
        role.wait_until_deleted()
        role = kube.load_role(f)

    role.create()
    role.wait_until_ready()
    role.refresh()

    role.delete()
    role.wait_until_deleted()


def test_install_operator(kube):

    import oci
    user = oci.config.from_file()['user']

    d = SparkOperator(namespace=NAMESPACE)
    if d.is_ready():
        d.delete()
        d.wait_until_deleted()
        d = SparkOperator(namespace=NAMESPACE)

    logging.info("Creating operator")
    d = SparkOperator(namespace=NAMESPACE)
    d.create(ociuser=user)
    d.wait_until_ready()
    d.refresh()

    pods = d.get_pods()
    assert len(pods) == 1

    p = pods[0]
    p.wait_until_ready(timeout=10)

    containers = p.get_containers()
    c = containers[0]
    assert len(c.get_logs()) != 0

    if not CLEANUP:
        return True

    d.delete()
    d.wait_until_deleted(timeout=20)


def _test_create_crd():
    """
    Test Creation of Operator CustomResourceDefinition
    :return:
    """

    f = get_yaml_path("custom-resource.yaml")
    crd = CustomResourceDefinition.load(f)


    if crd.is_ready():
        status = crd.delete()
        crd.wait_until_deleted()
        crd = CustomResourceDefinition.load(f)

    crd.create()
    crd.wait_until_ready()
    crd.refresh()

    crd.delete()
    crd.wait_until_deleted()

def test_check_sparkcluster(kube):
    """
    Test SparkCluster status
    :param kube:
    :return:
    """


    spark = SparkCluster.load(get_yaml_path("spark-cluster.yaml"))
    spark.namespace = 'spark'
    assert spark.is_ready()


def test_delete_spark_cluster(kube):

    assert SparkOperator(namespace=NAMESPACE).is_ready()

    spark = SparkCluster(namespace=NAMESPACE)

    assert  spark.is_ready()

    spark.delete()
    spark.wait_until_deleted()

    assert not spark.is_ready()

def test_install_spark(kube):

    from spark_on_oci import spark_controller
    import oci;
    config = oci.config.from_file()

    cluster_id = spark_controller.create_spark_cluster(
        config=config
        , cluster_name='spark1'
        , node_shape="VM.Standard1.4"
        , number_of_workers_per_ad=1
    )

    spark = spark_controller.install_spark(config=config,cluster_id=cluster_id,dry_run=False)
    spark.wait_until_ready()

    if CLEANUP:
        spark = spark_controller.uninstall_spark(config=config,cluster_id=cluster_id)
        spark.wait_until_deleted()

def test_create_sparkcluster(kube):

    assert SparkOperator(namespace=NAMESPACE).is_ready()

    spark = SparkCluster(namespace=NAMESPACE)

    if spark.is_ready():
        spark.delete()
        spark.wait_until_deleted()
        spark = SparkCluster(namespace=NAMESPACE)

    spark.create()
    spark.wait_until_ready()

def _test_operator(kube):

    f = get_yaml_path("operator.yaml")

    d = kube.load_deployment(f, set_namespace=False)
    d.namespace = "spark"
    if d.is_ready():
        d.delete(None)
        d.wait_until_deleted()
        d = kube.load_deployment(f,set_namespace=False)
        d.namespace = "spark"

    d.create()
    d.wait_until_ready(timeout=None)
    d.refresh()

    pods = d.get_pods()
    assert len(pods) == 1

    p = pods[0]
    p.wait_until_ready(timeout=10)

    containers = p.get_containers()
    c = containers[0]
    assert len(c.get_logs()) != 0

    kube.delete(d)
    d.wait_until_deleted(timeout=20)