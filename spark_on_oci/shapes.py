# Adapted from example in "Web Scraping with Python, 2nd Edition" by Ran Mitchell.
import csv

def scrape_shapes_to_csv(filename='oci_shapes.csv'):
    """
    Download the shape pricing table to a CSV file
    Requires that BeatifulSoup4 module is installed (pip install bs4)
    :param filename: where to save the CSV
    :return:
    """
    from urllib.request import urlopen
    from bs4 import BeautifulSoup
    html = urlopen("https://cloud.oracle.com/compute/pricing")
    soup = BeautifulSoup(html, "html.parser")
    vm_table, bm_table = soup.findAll("table", {"class": "responsiveTable"})[:2]
    rows = vm_table.findAll("tr")[1:] + bm_table.findAll('tr')[1:]
    title_row = ['instance_type', 'shape', 'ocpu_per_hour', 'ocpu', 'memory', 'network', 'storage', 'additional']
    with open(filename, "wt+", newline="") as f:
        writer = csv.writer(f)
        writer.writerow(title_row)

        for r, row in enumerate(rows):
            csv_row = []
            for c, cell in enumerate(row.findAll(["td", "th"])):
                text = cell.get_text().strip('\n')
                if c == 3:
                    for t in text.split('\n'):
                        csv_row.append(t)
                else:
                    csv_row.append(text)
            writer.writerow(csv_row)


def shapes_csv_to_dict(filename='oci_shapes.csv'):
    """
    Generate a dictionary from shape->shape-details from the
    CSV created by scrape_shapes_to_csv
    :param filename: which file
    :return: a dictionary of downloaded shapes
    """
    with open(filename, mode='r') as infile:
        reader = csv.reader(infile)
        shapes = {}
        for r, row in enumerate(reader):
            if r == 0:
                continue
            shape_name = row[1]

            shapes[shape_name] = {
                'instance_type': row[0],
                'ocpu': int(row[3].split(':')[1]),
                'memory': int(row[4].split(' ')[1]),
                'network': row[5].replace('Network: ', ''),
                'storage': row[6],
                'additional': row[7] if len(row) >= 8 else ''
            }

        return shapes


#######################
# OKE Available Shapes
#######################
class OCIShape:

    def __init__(self, name, ocpu, memory, storage, network, dense_io=False):
        self._name = name
        self._ocpu = ocpu
        self._memory = memory
        self._dense_io = dense_io
        self._storage = storage
        self._network = network

    def name(self):
        return self._name

    def cpu(self):
        return self._ocpu

    def memory(self):
        return self._memory

    def storage(self):
        return self._storage

    def dense_io(self):
        return self._dense_io

    def network(self):
        return self._network


def list_available_shape_names(config):
    """
    List available shapes in the current tenancy
    :param config:
    :return: list of strings of shape names
    """

    from oci.core import compute_client
    cc = compute_client.ComputeClient(config=config)
    shapes = cc.list_shapes(compartment_id=config['tenancy']).data
    return set([s.shape for s in shapes])


def get_shape_by_name(name):
    """
    Get details for a certain shape
    :param name: name of the shape
    :return: None or OCIShape
    """

    if name in SHAPES:
        details = SHAPES[name]
        return OCIShape(name,
                        ocpu=details['ocpu'],
                        memory=details['memory'],
                        dense_io='Dense' in details['instance_type'],
                        storage=details['storage'] + '. ' + details['additional'],
                        network=details['network']
                        )
    return None


# This dictionary is copied from the output of the `shapes_csv_to_dict` function above

SHAPES = {'VM.Standard.E2.1': {'instance_type': 'Standard',
  'ocpu': 1,
  'memory': 8,
  'network': '700 Mbps',
  'storage': 'Storage: Up to 1 PB of remote Block Volumes',
  'additional': ''},
 'VM.Standard2.1': {'instance_type': 'Standard',
  'ocpu': 1,
  'memory': 15,
  'network': '1 Gbps',
  'storage': 'Storage: Up to 1 PB of remote Block Volumes',
  'additional': ''},
 'VM.Standard.E2.2': {'instance_type': 'Standard',
  'ocpu': 2,
  'memory': 16,
  'network': '1.4 Gbps',
  'storage': 'Storage: Up to 1 PB of remote Block Volumes',
  'additional': ''},
 'VM.Standard2.2': {'instance_type': 'Standard',
  'ocpu': 2,
  'memory': 30,
  'network': '2 Gbps',
  'storage': 'Storage: Up to 1 PB of remote Block Volumes',
  'additional': ''},
 'VM.Standard.E2.4': {'instance_type': 'Standard',
  'ocpu': 4,
  'memory': 32,
  'network': '2.8 Gbps',
  'storage': 'Storage: Up to 1 PB of remote Block Volumes',
  'additional': ''},
 'VM.Standard2.4': {'instance_type': 'Standard',
  'ocpu': 4,
  'memory': 60,
  'network': '4.1 Gbps',
  'storage': 'Storage: Up to 1 PB of remote Block Volumes',
  'additional': ''},
 'VM.Standard.E2.8': {'instance_type': 'Standard',
  'ocpu': 8,
  'memory': 64,
  'network': '5.6 Gbps',
  'storage': 'Storage: Up to 1 PB of remote Block Volumes',
  'additional': ''},
 'VM.Standard2.8': {'instance_type': 'Standard',
  'ocpu': 8,
  'memory': 120,
  'network': '8.2 Gbps',
  'storage': 'Storage: Up to 1 PB of remote Block Volumes',
  'additional': ''},
 'VM.Standard2.16': {'instance_type': 'Standard',
  'ocpu': 16,
  'memory': 240,
  'network': '16.4 Gbps',
  'storage': 'Storage: Up to 1 PB of remote Block Volumes',
  'additional': ''},
 'VM.Standard2.24': {'instance_type': 'Standard',
  'ocpu': 24,
  'memory': 320,
  'network': '24.6 Gbps',
  'storage': 'Storage: Up to 1 PB of remote Block Volumes',
  'additional': ''},
 'VM.DenseIO2.8': {'instance_type': 'DenseIO',
  'ocpu': 8,
  'memory': 120,
  'network': '8.2 Gbps',
  'storage': 'Local Disk: 6.4 TB NVMe SSD',
  'additional': 'Additional Storage: Up to 1 PB of remote Block Volumes'},
 'VM.DenseIO2.16': {'instance_type': 'DenseIO',
  'ocpu': 16,
  'memory': 240,
  'network': '16.4 Gbps',
  'storage': 'Local Disk: 12.8 TB NVMe SSD',
  'additional': 'Additional Storage: Up to 1 PB of remote Block Volumes'},
 'VM.DenseIO2.24': {'instance_type': 'DenseIO',
  'ocpu': 24,
  'memory': 320,
  'network': '24.6 Gbps',
  'storage': 'Local Disk: 25.6 TB NVMe SSD',
  'additional': 'Additional Storage: Up to 1 PB of remote Block Volumes'},
 'BM.Standard.E2.64': {'instance_type': 'Standard',
  'ocpu': 64,
  'memory': 512,
  'network': '2 x 25 Gbps',
  'storage': 'Storage: Up to 1 PB of remote Block Volumes',
  'additional': ''},
 'BM.Standard2.52': {'instance_type': 'Standard',
  'ocpu': 52,
  'memory': 768,
  'network': '2 x 25 Gbps',
  'storage': 'Storage: Up to 1 PB of remote Block Volumes',
  'additional': ''},
 'BM.Standard.B1.44': {'instance_type': 'Standard',
  'ocpu': 44,
  'memory': 512,
  'network': '1 x 25 Gbps',
  'storage': 'Storage: Up to 1 PB of remote Block Volumes',
  'additional': ''},
 'BM.DenseIO2.52': {'instance_type': 'Dense I/O',
  'ocpu': 52,
  'memory': 768,
  'network': '2 x 25 Gbps',
  'storage': 'Local Disk: 51.2 TB NVMe SSD',
  'additional': 'Additional Storage: Up to 1 PB of remote Block Volumes'},
 'BM.HPC2.36': {'instance_type': 'HPC',
  'ocpu': 36,
  'memory': 384,
  'network': '1 x 25 Gbps and 1 x 100 Gbps RDMA',
  'storage': 'Local Disk: 6.7 TB NVMe local storage',
  'additional': 'Additional Storage: Up to 1 PB of remote Block Volumes'}
               }

if __name__ == "__main__":
    scrape_shapes_to_csv()
    print(shapes_csv_to_dict())