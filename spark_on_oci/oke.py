from __future__ import print_function
import oci
from kubernetes import client as kclient, config as kcconfig
from tempfile import NamedTemporaryFile


# This script demonstrates some of the Container Engine operations.
# Please review the documentation for more information about
# how Container Engine works, including permissions needed.
#
# https://docs.us-phoenix-1.oraclecloud.com/Content/ContEng/Concepts/contengoverview.htm

NODE_IMAGE_NAME="Oracle-Linux-7.4"
NODE_SHAPE="VM.Standard2.1"
#############################
# Container Engine operations
#############################

def _create_cluster(compartment_id, ce_client, vcn, cluster_name):
    """
    create_cluster
    This function demonstrates the process of creating a cluster.  The cidrs
    and other values are just for demonstration purposes.
    """
    success = True

    kubernetes_network_config = oci.container_engine.models.KubernetesNetworkConfig(pods_cidr="10.244.0.0/16",
                                                                                    services_cidr="10.96.0.0/16")

    cluster_create_options = oci.container_engine.models.ClusterCreateOptions(service_lb_subnet_ids=vcn['lb_subnets'],
                                                                              kubernetes_network_config=kubernetes_network_config)

    cluster_details = oci.container_engine.models.CreateClusterDetails(name=cluster_name,
                                                                       compartment_id=compartment_id,
                                                                       vcn_id=vcn['id'],
                                                                       kubernetes_version=get_kubernetes_version(
                                                                           ce_client),
                                                                       options=cluster_create_options)

    ce_composite_ops = oci.container_engine.ContainerEngineClientCompositeOperations(ce_client)

    response = ce_composite_ops.create_cluster_and_wait_for_state(cluster_details,
                                                                  wait_for_states=[
                                                                      oci.container_engine.models.WorkRequest.STATUS_SUCCEEDED,
                                                                      oci.container_engine.models.WorkRequest.STATUS_FAILED])

    # response.data is a WorkRequestSummary
    # gather the created resources from the work request
    resources = {}
    for resource in response.data.resources:
        print("{}: {}".format(resource.entity_type, resource.identifier))
        resources[resource.entity_type] = resource.identifier

    # If the workrequest failed, get the work request errors.
    if response.data.status == oci.container_engine.models.WorkRequest.STATUS_FAILED:
        get_work_request_errors(ce_client, compartment_id, response.data.id)
        success = False
    else:
        print(f"Create cluster {cluster_name} succeeded")

    # Get the work request logs
    print_header("Work request logs:")
    response = ce_client.list_work_request_logs(compartment_id, response.data.id)
    print(response.data)

    return success, resources


def update_cluster(ce_client, cluster_id):
    """
    update_cluster
    Currently there are two items you can update on the cluster the name and the kubernetes version.
    This function demonstrates updating the name.  Updating the kubernetes version works in
    a similar fashion.
    """

    update_cluster_details = oci.container_engine.models.UpdateClusterDetails(name="PythonSDK_cluster_1")

    ce_composite_ops = oci.container_engine.ContainerEngineClientCompositeOperations(ce_client)

    response = ce_composite_ops.update_cluster_and_wait_for_state(cluster_id,
                                                                  update_cluster_details,
                                                                  wait_for_states=[
                                                                      oci.container_engine.models.WorkRequest.STATUS_SUCCEEDED,
                                                                      oci.container_engine.models.WorkRequest.STATUS_FAILED])

    # If the workrequest failed, get the work request errors.
    if response.data.status == oci.container_engine.models.WorkRequest.STATUS_FAILED:
        get_work_request_errors(ce_client, compartment_id, response.data.id)
    else:
        print("Update cluster succeeded")

    return





def get_kubeconfig(ce_client, cluster_id):
    """
    get_kubeconfig
    Given a cluster id, retrieve the kubconfig.
    """

    response = ce_client.create_kubeconfig(cluster_id)

    # response.data.text contains the contents of the kubeconfig file which
    # can be writen to a file using code like the following snippet.
    """
    with open('kubconfig.txt', 'w') as f:
        f.write(response.data.text)
    """
    if response.data.text:
        print("kubeconfig retrieved")
    else:
        print("Error retrieving the kubeconfig")

    return response.data.text


def create_node_pool(compartment_id, ce_client, cluster_id, subnets, nodepool_name
                     , quantity_per_subnet
                     , node_image_name
                     , node_shape
                     , initial_node_labels_dict={"created_by":"oke_create_node_pool"}
                     , ssh_public_key=None):
    """
    create_node_pool
    Creates a node pool inside of a cluser
    """

    initial_node_labels = [{"key": k, "value": v} for (k, v) in initial_node_labels_dict.items()]

    success = True
    node_pool_create_details = oci.container_engine.models.CreateNodePoolDetails(compartment_id=compartment_id,
                                                                                 cluster_id=cluster_id,
                                                                                 name=nodepool_name,
                                                                                 kubernetes_version=get_kubernetes_version(
                                                                                     ce_client),
                                                                                 node_image_name=node_image_name,
                                                                                 node_shape=node_shape,
                                                                                 initial_node_labels=initial_node_labels,
                                                                                 quantity_per_subnet=quantity_per_subnet,
                                                                                 subnet_ids=subnets,
                                                                                 ssh_public_key=ssh_public_key)

    ce_composite_ops = oci.container_engine.ContainerEngineClientCompositeOperations(ce_client)

    response = ce_composite_ops.create_node_pool_and_wait_for_state(node_pool_create_details,
                                                                    wait_for_states=[
                                                                        oci.container_engine.models.WorkRequest.STATUS_SUCCEEDED,
                                                                        oci.container_engine.models.WorkRequest.STATUS_FAILED])

    # gather the created resources from the work request
    resources = {}
    for resource in response.data.resources:
        resources[resource.entity_type] = resource.identifier

    # If the workrequest failed, get the work request errors.
    if response.data.status == oci.container_engine.models.WorkRequest.STATUS_FAILED:
        get_work_request_errors(ce_client, compartment_id, response.data.id)
        success = False
    else:
        print(r"Create node pool { nodepool_name } succeeded")

    return success, resources


def update_node_pool(compartment_id, ce_client, node_pool_id):
    """
    update_node_pool
    Currently there are a number of features that can be updated in the node pool.
    This example will only update the name.  Please see the documentation for
    the other features which can be updated
    """

    update_node_pool_details = oci.container_engine.models.UpdateNodePoolDetails(name="PythonSDK_noodpool_1")

    ce_composite_ops = oci.container_engine.ContainerEngineClientCompositeOperations(ce_client)

    response = ce_composite_ops.update_node_pool_and_wait_for_state(node_pool_id,
                                                                    update_node_pool_details,
                                                                    wait_for_states=[
                                                                        oci.container_engine.models.WorkRequest.STATUS_SUCCEEDED,
                                                                        oci.container_engine.models.WorkRequest.STATUS_FAILED])

    if response.data.status == oci.container_engine.models.WorkRequest.STATUS_FAILED:
        get_work_request_errors(ce_client, compartment_id, response.data.id)
    else:
        print("Update node pool succeeded")

    return


def delete_node_pool(ce_client, node_pool_id):
    """
    delete_node_pool
    Deletes the specified node pool.  The cluster is not deleted.
    """
    ce_composite_ops = oci.container_engine.ContainerEngineClientCompositeOperations(ce_client)

    response = ce_composite_ops.delete_node_pool_and_wait_for_state(node_pool_id,
                                                                    wait_for_states=[
                                                                        oci.container_engine.models.WorkRequest.STATUS_SUCCEEDED])

    if response.status == 200:
        print("Node pool deleted successfully")
    else:
        print("Recieved '{}' when attempting to delete the node pool".format(response.status))

    return



def get_kubernetes_version(ce_client):
    """
    get_kubernetes_version
    Get the supported kubernetes versions from the service.  There are multiple
    versions supported but for the example we will just use the last one in the
    list.
    """
    response = ce_client.get_cluster_options(cluster_option_id="all")

    versions = response.data.kubernetes_versions
    if len(versions) > 0:
        kubernetes_version = versions[-1]
    else:
        raise RuntimeError("No supported Kubernetes versions found")

    return kubernetes_version


################
# VCN operations
################


def create_vcn(compartment_id, vn_client, availability_domains
               , display_name
               , tags={"create-by":"oke.create_vcn"}):
    """
    create_vcn
    See https://docs.us-phoenix-1.oraclecloud.com/Content/ContEng/Concepts/contengnetworkconfig.htm#VCN
    for details on how the VCN should be configured for container engine.
    This function will build a Virtual Cloud Network based on the example network resource configuration:
    https://docs.us-phoenix-1.oraclecloud.com/Content/ContEng/Concepts/contengnetworkconfigexample.htm
    The function returns a dictionary containing the id of the VCN created, a list of worker subnets and
    a list of load balancer subnets
    """


    vcn = {'id': None,
           'worker_subnets': [],
           'lb_subnets': []}

    subnet_template = "10.0.{}.0/24"

    number_of_lb_subnets = 2
    if number_of_lb_subnets > len(availability_domains):
        number_of_lb_subnets = 1

    vn_composite_ops = oci.core.VirtualNetworkClientCompositeOperations(vn_client)

    vcn_details = oci.core.models.CreateVcnDetails(cidr_block='10.0.0.0/16',
                                                   display_name=display_name,
                                                   dns_label='cevcn',
                                                   compartment_id=compartment_id,
                                                   freeform_tags=tags
                                                   )

    result = vn_composite_ops.create_vcn_and_wait_for_state(vcn_details,
                                                            wait_for_states=[
                                                                oci.core.models.Vcn.LIFECYCLE_STATE_AVAILABLE])

    vcn['id'] = result.data.id
    print("VCN Id: {}".format(vcn['id']))

    # Setup the gateway
    gateway_details = oci.core.models.CreateInternetGatewayDetails(compartment_id=compartment_id,
                                                                   display_name=display_name + '-gateway-0',
                                                                   is_enabled=True,
                                                                   freeform_tags=tags,
                                                                   vcn_id=vcn['id'])

    result = vn_composite_ops.create_internet_gateway_and_wait_for_state(gateway_details,
                                                                         wait_for_states=[
                                                                             oci.core.models.InternetGateway.LIFECYCLE_STATE_AVAILABLE])

    gateway_id = result.data.id
    print('Gateway Id: {}'.format(gateway_id))

    # Setup the route table
    route_table_rule = oci.core.models.RouteRule(cidr_block='0.0.0.0/0',
                                                 network_entity_id=gateway_id)

    route_table_details = oci.core.models.CreateRouteTableDetails(compartment_id=compartment_id,
                                                                  display_name=display_name + '-routetable-0',
                                                                  route_rules=[route_table_rule],
                                                                  freeform_tags=tags,
                                                                  vcn_id=vcn['id'])

    result = vn_composite_ops.create_route_table_and_wait_for_state(route_table_details,
                                                                    wait_for_states=[
                                                                        oci.core.models.RouteTable.LIFECYCLE_STATE_AVAILABLE])

    route_table_id = result.data.id
    print('Route Table Id: {}'.format(route_table_id))

    ################
    # Security Lists and Security Rules
    # More information on the security list configuration for container engine can be found here:
    # https://docs.us-phoenix-1.oraclecloud.com/Content/ContEng/Concepts/contengnetworkconfig.htm#securitylistconfig
    ################

    # Load balancer security rules
    load_balancer_egress_rule = oci.core.models.EgressSecurityRule(destination='0.0.0.0/0',
                                                                   destination_type=oci.core.models.EgressSecurityRule.DESTINATION_TYPE_CIDR_BLOCK,
                                                                   is_stateless=True,
                                                                   protocol='6',
                                                                   tcp_options=oci.core.models.TcpOptions())

    load_balancer_ingress_rule = oci.core.models.IngressSecurityRule(source='0.0.0.0/0',
                                                                     source_type=oci.core.models.IngressSecurityRule.SOURCE_TYPE_CIDR_BLOCK,
                                                                     is_stateless=True,
                                                                     protocol='6',
                                                                     tcp_options=oci.core.models.TcpOptions())

    load_balancers_security_list_details = oci.core.models.CreateSecurityListDetails(compartment_id=compartment_id,
                                                                                     display_name=display_name + '-LB-SecurityList',
                                                                                     egress_security_rules=[
                                                                                         load_balancer_egress_rule],
                                                                                     ingress_security_rules=[
                                                                                         load_balancer_ingress_rule],
                                                                                     freeform_tags=tags,
                                                                                     vcn_id=vcn['id'])

    result = vn_composite_ops.create_security_list_and_wait_for_state(load_balancers_security_list_details,
                                                                      wait_for_states=[
                                                                          oci.core.models.SecurityList.LIFECYCLE_STATE_AVAILABLE])

    load_balancer_security_list_id = result.data.id
    print('Load Balancer Security List Id: {}'.format(load_balancer_security_list_id))

    # Worker security rules
    worker_egress_rules = []

    number_of_worker_subnets = len(availability_domains)

    for i in range(number_of_worker_subnets):
        destination = subnet_template.format(10 + i)
        worker_egress_rules.append(oci.core.models.EgressSecurityRule(destination=destination,
                                                                      destination_type=oci.core.models.EgressSecurityRule.DESTINATION_TYPE_CIDR_BLOCK,
                                                                      is_stateless=True,
                                                                      protocol='all'))

    worker_egress_rules.append(oci.core.models.EgressSecurityRule(destination='0.0.0.0/0',
                                                                  destination_type=oci.core.models.EgressSecurityRule.DESTINATION_TYPE_CIDR_BLOCK,
                                                                  is_stateless=False,
                                                                  protocol='all'))
    worker_ingress_rules = []

    for i in range(number_of_worker_subnets):
        source = subnet_template.format(10 + i)
        worker_ingress_rules.append(oci.core.models.IngressSecurityRule(source=source,
                                                                        source_type=oci.core.models.IngressSecurityRule.SOURCE_TYPE_CIDR_BLOCK,
                                                                        is_stateless=True,
                                                                        protocol='all'))

    worker_ingress_rules.append(oci.core.models.IngressSecurityRule(source='0.0.0.0/0',
                                                                    source_type=oci.core.models.IngressSecurityRule.SOURCE_TYPE_CIDR_BLOCK,
                                                                    is_stateless=False,
                                                                    protocol='1',
                                                                    icmp_options=oci.core.models.IcmpOptions(type=3,
                                                                                                             code=4)))

    worker_ingress_rules.append(oci.core.models.IngressSecurityRule(source='130.35.0.0/16',
                                                                    source_type=oci.core.models.IngressSecurityRule.SOURCE_TYPE_CIDR_BLOCK,
                                                                    is_stateless=False,
                                                                    protocol='6',
                                                                    tcp_options=oci.core.models.TcpOptions(
                                                                        destination_port_range=oci.core.models.PortRange(
                                                                            min=22, max=22))))

    worker_ingress_rules.append(oci.core.models.IngressSecurityRule(source='138.1.0.0/17',
                                                                    source_type=oci.core.models.IngressSecurityRule.SOURCE_TYPE_CIDR_BLOCK,
                                                                    is_stateless=False,
                                                                    protocol='6',
                                                                    tcp_options=oci.core.models.TcpOptions(
                                                                        destination_port_range=oci.core.models.PortRange(
                                                                            min=22, max=22))))

    worker_ingress_rules.append(oci.core.models.IngressSecurityRule(source='0.0.0.0/0',
                                                                    source_type=oci.core.models.IngressSecurityRule.SOURCE_TYPE_CIDR_BLOCK,
                                                                    is_stateless=False,
                                                                    protocol='6',
                                                                    tcp_options=oci.core.models.TcpOptions(
                                                                        destination_port_range=oci.core.models.PortRange(
                                                                            min=22, max=22))))

    # Expose Kubernetes default NodePort range
    worker_ingress_rules.append(oci.core.models.IngressSecurityRule(source='0.0.0.0/0',
                                                                    source_type=oci.core.models.IngressSecurityRule.SOURCE_TYPE_CIDR_BLOCK,
                                                                    is_stateless=False,
                                                                    protocol='6',
                                                                    tcp_options=oci.core.models.TcpOptions(
                                                                        destination_port_range=oci.core.models.PortRange(
                                                                            min=30000, max=32767))))

    worker_security_list_details = oci.core.models.CreateSecurityListDetails(compartment_id=compartment_id,
                                                                             display_name=display_name + '-Worker-SecurityList',
                                                                             egress_security_rules=worker_egress_rules,
                                                                             ingress_security_rules=worker_ingress_rules,
                                                                             freeform_tags=tags,
                                                                             vcn_id=vcn['id'])

    result = vn_composite_ops.create_security_list_and_wait_for_state(worker_security_list_details,
                                                                      wait_for_states=[
                                                                          oci.core.models.SecurityList.LIFECYCLE_STATE_AVAILABLE])

    worker_security_list_id = result.data.id
    print('Worker Security List Id: {}'.format(worker_security_list_id))

    ################
    # Subnets
    # More information on the subnet configuration for container engine can be found here:
    # https://docs.us-phoenix-1.oraclecloud.com/Content/ContEng/Concepts/contengnetworkconfig.htm#subnetconfig
    ################

    # Worker subnets
    display_name_template = display_name + '-workers-{}'
    dns_label_template = 'workers{}'
    for i in range(number_of_worker_subnets):
        cidr_block = subnet_template.format(10 + i)
        display_name = display_name_template.format(1 + i)
        dns_label = dns_label_template.format(1 + i)

        subnet_details = oci.core.models.CreateSubnetDetails(availability_domain=availability_domains[i],
                                                             compartment_id=compartment_id,
                                                             cidr_block=cidr_block,
                                                             display_name=display_name,
                                                             dns_label=dns_label,
                                                             vcn_id=vcn['id'],
                                                             route_table_id=route_table_id,
                                                             security_list_ids=[worker_security_list_id],
                                                             freeform_tags=tags)

        result = vn_composite_ops.create_subnet_and_wait_for_state(subnet_details,
                                                                   wait_for_states=[
                                                                       oci.core.models.Subnet.LIFECYCLE_STATE_AVAILABLE])
        vcn['worker_subnets'].append(result.data.id)

    print("Worker Subnets: {}".format(vcn['worker_subnets']))

    # Load balancer subnets
    display_name_template = display_name + '-loadbalancers-{}'
    dns_label_template = 'loadbalancers{}'
    for i in range(number_of_lb_subnets):
        cidr_block = subnet_template.format(20 + i)
        display_name = display_name_template.format(1 + i)
        dns_label = dns_label_template.format(1 + i)
        subnet_details = oci.core.models.CreateSubnetDetails(availability_domain=availability_domains[i],
                                                             compartment_id=compartment_id,
                                                             cidr_block=cidr_block,
                                                             display_name=display_name,
                                                             dns_label=dns_label,
                                                             vcn_id=vcn['id'],
                                                             route_table_id=route_table_id,
                                                             security_list_ids=[load_balancer_security_list_id],
                                                             freeform_tags=tags)

        result = vn_composite_ops.create_subnet_and_wait_for_state(subnet_details,
                                                                   wait_for_states=[
                                                                       oci.core.models.Subnet.LIFECYCLE_STATE_AVAILABLE])
        vcn['lb_subnets'].append(result.data.id)

    print("Loadbalancer Subnets: {}".format(vcn['lb_subnets']))

    return vcn


def delete_vcn(vn_client, compartment_id, vcn_id):
    """
    delete_vcn
    Deletes the example VCN.  There are more resources associated
    with the VCN than passed in with the vcn_resources dictionary.  Those
    resources will be discovered and deleted.
    """

    vn_composite_ops = oci.core.VirtualNetworkClientCompositeOperations(vn_client)

    subnets = vn_client.list_subnets(compartment_id=compartment_id,vcn_id=vcn_id).data

    # Delete the load balancer subnets
    print("Deleting load balancer and worker subnets...")
    for subnet in subnets:
        vn_composite_ops.delete_subnet_and_wait_for_state(subnet.id,
                                                          wait_for_states=[
                                                              oci.core.models.Subnet.LIFECYCLE_STATE_TERMINATED])

    # VCNs have default security lists, route tables which cannot be deleted.
    # Get the details of the VCN to get the default security list and default route table ids so
    # they can be skipped when getting the security lists and route tables
    response = vn_client.get_vcn(vcn_id)
    default_security_list_id = response.data.default_security_list_id
    default_route_table_id = response.data.default_route_table_id

    # Retrieve and delete the security lists
    print("Deleting security lists...")
    # We could just retrieve all of the security lists, but there may be cases where not all of them
    # would come back in a single page.  Here is another example of using the pagaination to
    # retrieve items from a list call.  More examples are in pagination.py
    for security_list in oci.pagination.list_call_get_all_results_generator(vn_client.list_security_lists,
                                                                            'record',
                                                                            compartment_id,
                                                                            vcn_id,
                                                                            lifecycle_state=oci.core.models.SecurityList.LIFECYCLE_STATE_AVAILABLE):
        if security_list.id != default_security_list_id:
            vn_composite_ops.delete_security_list_and_wait_for_state(security_list.id,
                                                                     wait_for_states=[
                                                                         oci.core.models.SecurityList.LIFECYCLE_STATE_TERMINATED])

    # Retrieve and delete the route tables
    print("Deleting route tables...")
    for route_table in oci.pagination.list_call_get_all_results_generator(vn_client.list_route_tables,
                                                                          'record',
                                                                          compartment_id,
                                                                          vcn_id,
                                                                          lifecycle_state=oci.core.models.RouteTable.LIFECYCLE_STATE_AVAILABLE):
        if route_table.id != default_route_table_id:
            vn_composite_ops.delete_route_table_and_wait_for_state(route_table.id,
                                                                   wait_for_states=[
                                                                       oci.core.models.RouteTable.LIFECYCLE_STATE_TERMINATED])

    # Retrieve and delete the gateway
    print("Deleting internet gateways...")
    for gateway in oci.pagination.list_call_get_all_results_generator(vn_client.list_internet_gateways,
                                                                      'record',
                                                                      compartment_id,
                                                                      vcn_id,
                                                                      lifecycle_state=oci.core.models.InternetGateway.LIFECYCLE_STATE_AVAILABLE):
        vn_composite_ops.delete_internet_gateway_and_wait_for_state(gateway.id, wait_for_states=[
            oci.core.models.InternetGateway.LIFECYCLE_STATE_TERMINATED])

    print("Deleting virtual cloud network...")
    response = vn_composite_ops.delete_vcn_and_wait_for_state(vcn_id,
                                                              wait_for_states=[
                                                                  oci.core.models.Vcn.LIFECYCLE_STATE_TERMINATED])

    print("VCN {} has been deleted".format(vcn_id))


#########
# Helpers
#########

def get_work_request_errors(ce_client, compartment_id, work_request_id):
    """
    get_work_request_errors
    Retrieves the errors for a work request and prints them.
    """
    print_header('Work request errors:')
    response = ce_client.list_work_request_errors(compartment_id, work_request_id)
    print(response.data)


def print_header(header):
    """
    print_header
    Prints a section header
    """

    print('\n')
    print(header)
    print("=" * len(header))


def init_clients(config):
    ce_client = oci.container_engine.ContainerEngineClient(config)
    vn_client = oci.core.VirtualNetworkClient(config)
    id_client = oci.identity.IdentityClient(config)
    return ce_client, vn_client, id_client


def get_tenancy_name(config):
    ce_client, vn_client, id_client = init_clients(config)
    tenancy_details = id_client.get_tenancy(tenancy_id=config['tenancy']).data
    return tenancy_details.name


def upgrade_tiller_version(kubeconfig_file, version="v2.12.0"):
    kcconfig.load_kube_config(config_file=kubeconfig_file)
    v1beta1 = kclient.ExtensionsV1beta1Api()
    tiller_deploy = v1beta1.read_namespaced_deployment(namespace='kube-system', name='tiller-deploy')

    new_tiller_image = f'gcr.io/kubernetes-helm/tiller:{version}'

    tiller_deploy.spec.template.spec.containers[0].image = new_tiller_image

    api_response = v1beta1.patch_namespaced_deployment(namespace='kube-system'
                                                       , name='tiller-deploy'
                                                       , body=tiller_deploy)

    # print(f"Tiller updated to version {version}.  Status={api_response.status}")

    return api_response


def create_cluster(config, cluster_name, workers_per_ad, availability_domains: list = None
                   , node_shape: str=NODE_SHAPE, tags: dict=None, ssh_public_key=None):

    if availability_domains is None:
        availability_domains = []

    if tags is None:
        tags = {}

    ce_client, vn_client, id_client = init_clients(config)
    compartment_id = config['tenancy']

    # Get the Availability Domains for the compartment
    found_availability_domains = id_client.list_availability_domains(compartment_id).data
    found_availabiilty_domains_names = [x.name for x in found_availability_domains]

    if availability_domains:
        for ad in availability_domains:
            if ad not in found_availabiilty_domains_names:
                print(f"Selected availability domain {ad} not found.  Please choose from list: {found_availabiilty_domains_names}")
                return None
    else:
        availability_domains = found_availabiilty_domains_names

    ##################
    # Create resources
    ##################
    print_header("Create the Virtual Cloud Network")

    vcn_resources = create_vcn(compartment_id, vn_client
                               , availability_domains
                               , cluster_name + '-vcn'
                               , tags=tags)

    print("VCN resources: {}".format(vcn_resources))
    print()
    print_header(f"Create the Cluster {cluster_name}")
    try:
        cluster_success, cluster_resources = _create_cluster(compartment_id
                                                        , ce_client
                                                        , vcn_resources
                                                        , cluster_name)

    except Exception as e:
        print(f"Unknown Exception {e} while creating cluster {cluster_name}")
        cluster_success = False
    else:
        print(f"Cluster resourse: {cluster_resources}")

    if cluster_success:
        print_header('Create a node pool')
        node_pool_success, node_pool_resources = create_node_pool(compartment_id, ce_client,
                                                                  cluster_resources['cluster'],
                                                                  vcn_resources['worker_subnets'],
                                                                  quantity_per_subnet=workers_per_ad,
                                                                  node_image_name=NODE_IMAGE_NAME,
                                                                  node_shape=node_shape,
                                                                  nodepool_name=cluster_name + '-nodepool',
                                                                  initial_node_labels_dict=tags,
                                                                  ssh_public_key=ssh_public_key)

        print("Node pool resources: {}".format(node_pool_resources))

        return cluster_resources['cluster']

    else:
        print(f"Unable to create cluster {cluster_name}, deleting vcn {vcn_resources}")
        delete_vcn(vn_client=vn_client,compartment_id=compartment_id,vcn_id=vcn_resources['id'])
        return None


def delete_cluster(config, cluster_id):
    ce_client, vn_client, id_client = init_clients(config)

    # Get cluster vcn name from details:
    cluster = ce_client.get_cluster(cluster_id=cluster_id).data

    # Note: Any exceptions caught while trying to clean up resources are printed, but not
    #       handled.
    if cluster_id:
        print_header("Delete the Cluster")
        try:
            ce_composite_ops = oci.container_engine.ContainerEngineClientCompositeOperations(ce_client)

            response = ce_composite_ops.delete_cluster_and_wait_for_state(cluster_id,
                                                                          wait_for_states=[
                                                                              oci.container_engine.models.WorkRequest.STATUS_SUCCEEDED])

            if response.status == 200:
                print("Cluster deleted successfully")
            else:
                print("Recieved '{}' when attempting to delete the cluster".format(response.status))
        except Exception as e:
            print("Failed to delete cluster: {}".format(cluster_id))
            print(e)

    print_header("Delete the Virtual Cloud Network")

    try:
        delete_vcn(vn_client, compartment_id=config["tenancy"], vcn_id=cluster.vcn_id)
    except Exception as e:
        print("Failed to delete VCN: {}".format(cluster.vcn_id))
        print(e)


def get_kube_config(config: dict, cluster_id: str):

    cec, vnc, idc = init_clients(config)

    kc = cec.create_kubeconfig(cluster_id=cluster_id).data.text
    temp_kube_config = NamedTemporaryFile()
    with open(temp_kube_config.name, 'w') as f:
        f.write(kc)

    return temp_kube_config


def get_cluster_endpoints(kubeconfig_file, chart_namespace):
    kcconfig.load_kube_config(config_file=kubeconfig_file)
    api = kclient.CoreV1Api()

    services = api.list_namespaced_service(namespace=chart_namespace, label_selector="chart")

    service_to_port = {}
    for service in services.items:
        if service.spec.type == 'NodePort':
            service_to_port[service.metadata.name] = service.spec.ports[0].node_port

    endpoints = api.list_namespaced_endpoints(namespace=chart_namespace, label_selector="chart")

    internal_to_external = get_external_ips()

    service_endpoints = []
    for endpoint in endpoints.items:

        service_name = endpoint.metadata.name

        if service_name in service_to_port:
            if not endpoint.subsets:
                continue
            if not endpoint.subsets[0].addresses:
                continue
            service_ip = endpoint.subsets[0].addresses[0].node_name

            service_ip = internal_to_external[service_ip]

            service_port = service_to_port[service_name]
            endpoint_info = (service_name, service_ip, service_port)
            service_endpoints.append(endpoint_info)

    return service_endpoints


def get_ingress_endpoints(kubeconfig_file, chart_namespace):
    """
    Returns dict of ingress -> url
    :param kubeconfig_file:
    :param chart_namespace:
    :return: dict of ingress name to url
    """
    kcconfig.load_kube_config(config_file=kubeconfig_file)
    api = kclient.CoreV1Api()

    services = api.list_service_for_all_namespaces(label_selector="component=ingress")
    ip = None
    if len(services.items):
        ip = services.items[0].status.load_balancer.ingress[0].ip

    api = kclient.ExtensionsV1beta1Api()
    ingresses = api.list_namespaced_ingress(namespace=chart_namespace, label_selector='chart')

    ingress_to_url = {}
    for ingress in ingresses.items:
        ingress_to_url[ingress.metadata.name] = f"{ip}{ingress.spec.rules[0].http.paths[0].path}"

    return ingress_to_url

def get_external_ips():
    api = kclient.CoreV1Api()
    nodes = api.list_node()

    internal_to_external = {}

    for n in nodes.items:
        for a in n.status.addresses:
            if a.type == 'ExternalIP':
                internal_to_external[n.metadata.name] = a.address

    return internal_to_external

def list_clusters(config, all=False):
    ce_client = oci.container_engine.ContainerEngineClient(config)
    clusters = ce_client.list_clusters(compartment_id=config['tenancy'])
    return clusters.data


def list_nodepools(config, cluster_id):
    cc, nv, ic = init_clients(config)
    all_nodepools = cc.list_node_pools(compartment_id=config['tenancy']).data

    cluster_nodepools = [nodepool for nodepool in all_nodepools if nodepool.cluster_id == cluster_id]
    return cluster_nodepools

# TODO: move this into generic get_config() function
def create_object_store_credentials(config,name):
    _,_,identity = init_clients(config)
    create_customer_secret_key = oci.identity.models.CreateCustomerSecretKeyDetails()
    create_customer_secret_key.display_name = name
    response = identity.create_customer_secret_key(create_customer_secret_key,config["user"])
    return response.data

def delete_object_store_credentials(config,name):
    _,_,identity = init_clients(config)
    keys = identity.list_customer_secret_keys(config['user']).data
    for k in keys:
        if k.display_name == name:
            identity.delete_customer_secret_key(config['user'],k.id)
            return True
    return False
