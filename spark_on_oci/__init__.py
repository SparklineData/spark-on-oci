#!/bin/bash
"""spark-on-oci -- Spark on Oracle Kubernetes Engine."""

__title__ = 'spark-on-oci'
__version__ = '0.0.3'
__description__ = 'Tools for running Spark on OCI.'
__author__ = 'Ali R'
__author_email__ = 'ali.rathore@oracle.com'
__url__ = 'https://gitlab.com/sparklinedata/spark-on-oci'


if __name__ == "__main__":
    from .cli import main
    main()
