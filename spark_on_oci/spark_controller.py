
from spark_on_oci import oke,shapes, spark_cluster
import shutil
from kubernetes import config as kconf

CLUSTER_TAGS = {"spark/controller-version": "0.0.1"}
DEFAULT_NODE_SHAPE = "VM.Standard2.16"
DEFAULT_NUM_WORKERS_PER_AD = 2
DEFAULT_SPARK_VERSION="2.3.1"
# TODO: change to create_or_get_oke_cluster
def create_spark_cluster(config : dict,
                         cluster_name : str,
                         availability_domains: list = None,
                         node_shape: str = DEFAULT_NODE_SHAPE,
                         number_of_workers_per_ad: int = DEFAULT_NUM_WORKERS_PER_AD,
                         ssh_public_key : str = None):
    """
    Create a Spark Cluster
    :param config: the oci config
    :param cluster_name: name of the cluster
    :param availability_domains: optional list of availability domains
    :param node_shape: shape of worker nodes
    :param number_of_workers_per_ad: number of workers per availability domains
    :param ssh_public_key: public ssh key for ssh-ing into workers
    :return: OCID of the OKE cluster
    """

    existing_clusters = list_spark_clusters(config)

    for cluster in existing_clusters:
        if cluster["cluster_name"] == cluster_name:
            print(f"Cluster with name '{cluster_name}' already exists.  Please choose a different name")
            return cluster['cluster_id']

    available_shapes = shapes.list_available_shape_names(config=config)
    if node_shape not in available_shapes:
        oke.print_header(f"""
             Shape {node_shape} is not available.  Please choose from
             one of {available_shapes}
         """)
        return None

    cluster_tags = CLUSTER_TAGS

    cluster_id = oke.create_cluster(config
                                    , cluster_name
                                    , number_of_workers_per_ad
                                    , availability_domains
                                    , node_shape=node_shape
                                    , tags=cluster_tags
                                    , ssh_public_key=ssh_public_key)

    if not cluster_id:
        print(f"Failed to create cluster {cluster_name}")
        return None

    return cluster_id

def create_loadbalancer(config, cluster_id):
    """
    Create an external IP for the cluster.
    :param config:
    :param cluster_id:
    :return:
    """

    kc = oke.get_kube_config(config, cluster_id)
    kconf.load_kube_config(kc.name)
    loadbalancer = spark_cluster.LoadBalancer(namespace='kube-system')

    if not loadbalancer.is_ready():
        loadbalancer.create(namespace='kube-system')
        print(loadbalancer.get_endpoints())

    return loadbalancer

def delete_loadbalancer(config, cluster_id):
    """
    Delete the external IP of the cluster
    """
    kc = oke.get_kube_config(config, cluster_id)
    kconf.load_kube_config(kc.name)
    loadbalancer = spark_cluster.LoadBalancer(namespace='kube-system')
    loadbalancer.delete(None)
    loadbalancer.wait_until_deleted()
    return loadbalancer

def install_spark(config, cluster_id
                  , number_of_workers: int = 0
                  , spark_properties: str = ""
                  , dry_run=False
                  , recreate=False
                  , pre_install=None
                  , executor_cores=None,executor_memory=None,executor_disk=None
                  , spark_version=DEFAULT_SPARK_VERSION
                  , expose_jdbc='http'
                  , snap_url=None
                  , auth=None):
    """
    Start Spark on an existing Cluster.

    Raises RuntimeError if cluster already exists

    :param config: the oci config
    :param cluster_id: OKE cluster OCID
    :param number_of_workers: number of Spark workers to create. If 0, will use 1 worker per node
    :param spark_properties: additional lines for the spark.properties files
    :param dry_run: returns the SparkCluster configuration file instead of installing spark
    :param recreate: uninstalls spark before installing.
    :return: SparkCluster
    """

    kc = oke.get_kube_config(config, cluster_id)
    kconf.load_kube_config(kc.name)
    spark = spark_cluster.SparkCluster()
    if (not dry_run) and spark.is_ready():
        if recreate:
            spark.delete()
            spark.wait_until_deleted()
            spark = spark_cluster.SparkCluster()
        else:
            return spark

    # TODO: can we remove this logic from spark_controller and put into the SparkThriftServer image? Using something
    # like https://docs.cloud.oracle.com/iaas/Content/Compute/Tasks/gettingmetadata.htm

    nodepools = oke.list_nodepools(config, cluster_id)

    if len(nodepools) != 1:
        raise RuntimeError("Only clusters with 1 nodepool are currently supported")

    nodepool = nodepools[0]

    # if number of workers is not specified by user, use entire nodepool
    if not number_of_workers:
        worker_per_subnet = nodepool.quantity_per_subnet
        number_of_subnets = len(nodepool.subnet_ids)
        number_of_workers = worker_per_subnet * number_of_subnets

    worker_shape = shapes.get_shape_by_name(nodepool.node_shape)

    if worker_shape:
        cores, memory, disk = calculate_cores_memory_disk(worker_shape)
    elif not executor_cores:
        raise (f"Worker shape {nodepool.node_shape} not known.  Please provide 'executor_cores' parameter")
    elif not executor_memory:
        raise (f"Worker shape {nodepool.node_shape} not known.  Please provide 'executor_memory' parameter")
    elif not executor_disk:
        raise (f"Worker shape {nodepool.node_shape} not known.  Please provide 'executor_disk' parameter")
    else:
        cores, memory, disk = executor_cores, executor_memory, executor_disk

    # Handle spark cluster resources
    spark.obj['spec']['worker']['instances'] = number_of_workers
    spark.obj['spec']['worker']['executorCores'] = cores
    spark.obj['spec']['worker']['executorMemory'] = f'{memory}g'
    spark.obj['spec']['executor']['cores'] = cores
    spark.obj['spec']['executor']['instances'] = number_of_workers
    spark.obj['spec']['executor']['memory'] = f'{memory}g'

    # Handle spark image version
    spark.obj["spec"]["Master"]["ImageTag"] = spark_version
    spark.obj["spec"]["Workers"]["ImageTag"] = spark_version
    spark.obj["spec"]["SparkApplication"]["ImageTag"] = spark_version

    application_name = f"SparkThriftServer-{spark_version}"

    # Handle snap
    if snap_url:
        spark.obj['spec']['SparkApplication']['InstallCommand'] = f"wget -qO- '{snap_url}' | tar xvz -C /app --strip-components=1"
        spark.obj['spec']['SparkApplication']['StartCommand'] = "/app/bin/snap-tool start"
        spark.obj['spec']['SparkApplication']['Env']['SNAP_PROPERTIES'] = "/conf/spark.properties"

        application_name = f"SNAP-{spark_version}"

    if expose_jdbc == 'binary':
        spark.obj["spec"]["SparkApplicationJDBC"]["ServiceType"] = "ClusterIP"
        spark.obj['spec']['SparkApplication']['hiveconf'] = ''
    elif expose_jdbc == 'insecure':
        spark.obj["spec"]["SparkApplicationJDBC"]["ServiceType"] = "NodePort"
        spark.obj['spec']['SparkApplication']['hiveconf'] = ''
    elif expose_jdbc == 'http':
        spark.obj['spec']['SparkApplication']['hiveconf'] = ' '.join([
            "--hiveconf hive.server2.transport.mode=http",
            "--hiveconf hive.server2.thrift.http.port=10000",
            "--hiveconf hive.server2.thrift.http.path=/jdbc",
            "--hiveconf hive.server2.thrift.http.cookie.auth.enabled=false"
        ])
    elif not expose_jdbc:
        pass
    else:
        raise NotImplementedError(f'expose_jdbc must be "binary", "insecure", or "http".  got "{expose_jdbc}"')

    if auth:
        spark.obj['spec']['auth']['type'] = 'basic'
        spark.obj['spec']['secrets']['auth']['basic']['auth'] = auth
    else:
        spark.obj['spec']['auth']['type'] = ''

    if pre_install:
        spark.obj['spec'] = pre_install(spark.obj['spec'])

    # handle spark properties file
    spark_properties += f"\nspark.hadoop.fs.oci.client.hostname=https://objectstorage.{config['region']}.oraclecloud.com"
    spark_properties += f"\nspark.app.name={application_name}"
    spark.obj['spec']['properties'] += spark_properties


    if dry_run:
        return spark

    operator = spark_cluster.SparkOperator()
    if not operator.is_ready():
        user = config['user']
        operator.create(ociuser=user)
        operator.wait_until_ready()

    spark.create()

    return spark


def uninstall_spark(config, cluster_id):
    """
    Uninstall Spark from a cluster
    :param config: the oci config
    :param cluster_id: id of the cluster to uninstall spark
    :return: information about the installed release
    """
    kc = oke.get_kube_config(config, cluster_id)
    kconf.load_kube_config(kc.name)
    spark = spark_cluster.SparkCluster()

    return spark.delete()

def restart_spark_operator(config, cluster_id):
    """
    Restart the Spark Operator
    :param config: the oci config
    :return: Restart the Spark Operator pod (this will fetch the latest helm changes)
    """
    kc = oke.get_kube_config(config, cluster_id)
    kconf.load_kube_config(kc.name)

    spark = spark_cluster.SparkCluster()
    if spark.is_ready():
        raise RuntimeError("Spark is already running. Please uninstall before restarting Operator")

    operator = spark_cluster.SparkOperator()
    if operator.is_ready():
        pods = operator.get_pods()
        for p in pods:
            p.delete()

    return operator

def print_cluster_endpoints(config, cluster_name, namespace=spark_cluster.DEFAULT_NAMESPACE) -> dict:
    """
    Print the accessible URLS
    :param config:
    :param cluster_name:
    :return:
    """

    endpoints = list_spark_cluster_endpoints(config, cluster_name, namespace=namespace)

    for service, ip, port in endpoints:
        print(f"http://{ip}:{port} ({service}) ")

    return endpoints

def get_cluster_by_name(config: dict, cluster_name: str):
    """
    Get Cluster ID By name
    :param config: the configuration dict
    :param cluster_name: name of the cluster
    :param namespace: which k8s namespace
    :return:
    """
    existing_clusters = list_spark_clusters(config, cluster_name)
    cluster_id = None
    for cluster in existing_clusters:
        if cluster['cluster_name'] == cluster_name:
            cluster_id = cluster['cluster_id']
            return cluster_id

    if not cluster_id:
        return None

def list_spark_cluster_endpoints(config:dict, cluster_name: str, namespace=spark_cluster.DEFAULT_NAMESPACE):
    """
    Get list of accessible URLs for a cluster
    :param config: the configuration dict
    :param cluster_name: name of cluster
    :return: list of URLs
    """

    existing_clusters = list_spark_clusters(config)

    kc = None
    for cluster in existing_clusters:
        if cluster['cluster_name'] == cluster_name:
            kc = oke.get_kube_config(config, cluster_id=cluster['cluster_id'])
            break

    if not kc:
        return []

    return oke.get_cluster_endpoints(kc.name, namespace)

def list_spark_ingress(config:dict, cluster_name: str, namespace=spark_cluster.DEFAULT_NAMESPACE):
    """
    Get list of ingress URLs for a cluster
    :param config: the configuration dict
    :param cluster_name: name of cluster
    :return: dict of ingress names to URLs
    """

    existing_clusters = list_spark_clusters(config)

    kc = None
    for cluster in existing_clusters:
        if cluster['cluster_name'] == cluster_name:
            kc = oke.get_kube_config(config, cluster_id=cluster['cluster_id'])
            break

    if not kc:
        return []

    return oke.get_ingress_endpoints(kc.name, namespace)

def delete_spark_cluster(config: dict, cluster_name: str, force : bool = False):
    """
    Gracefully delete a Spark cluster

    Shutsdown Spark then deletes the cluster and networking resources
    :param config: the oci config
    :param cluster_name: name of the cluster
    :return: True if cluster deleted
    """

    existing_clusters = list_spark_clusters(config)

    cluster_id = None
    for cluster in existing_clusters:
        if cluster['cluster_name'] == cluster_name:
            cluster_id = cluster['cluster_id']
            break

    if not cluster_id:
        oke.print_header(f"Spark cluster '{cluster_name}' not found")
        return False

    try:
        uninstalled = uninstall_spark(config, cluster_id)
    except Exception as e:
        if not force:
            print("Unable to uninstall Spark before deletion")
            raise e
    else:
        print(f"uninstalled '{uninstalled['metadata']['name']}' from namespace '{uninstalled['metadata']['namespace']}'")

    oke.delete_cluster(config, cluster_id)
    return True


def list_spark_clusters(config, verbose=False):
    cec, vnc, idc = oke.init_clients(config)

    # First get the VNCs associated with the spark cluster using CLUSTER_TAGS
    vcn_list = vnc.list_vcns(compartment_id=config["tenancy"]).data
    spark_vcns = {}
    for v in vcn_list:
        for k in CLUSTER_TAGS.keys():
            if k in v.freeform_tags.keys():
                spark_vcns[v.id] = v

    spark_vcn_ids = spark_vcns.keys()

    # Now get the OKE clusters bound the those VNCs
    spark_clusters = []
    for cluster in cec.list_clusters(compartment_id=config["tenancy"]).data:
        if cluster.vcn_id in spark_vcn_ids:
            cluster_info = {"cluster_id": cluster.id
                            , "vcn_id": cluster.vcn_id
                            , "cluster_name": cluster.name
                            , "network_name": spark_vcns[cluster.vcn_id].display_name
                            , "lifecycle_state": cluster.lifecycle_state
                            , "time_created": str(cluster.metadata.time_created)}
            if verbose:
                cluster_info["cluster_details"] = cluster

            spark_clusters += [cluster_info]

    return spark_clusters


def calculate_cores_memory_disk(shape: shapes.OCIShape):
    """
    Calculate the number of cores, memory and disk for each executor
    for a given shape
    :param shape: the OCI Shape
    :return: executor_cores, executor_memory, worker_disk
    """
    executor_cores = max(shape.cpu() - 1,1)
    executor_memory = max(shape.memory() - 7,3)
    worker_disk = 300

    return executor_cores, executor_memory, worker_disk

def get_kube_config(config, cluster_name, copy_to=None):
    """
    Get the kubernetes config file of a cluster
    :param config: oci config
    :param cluster_name: name of cluster
    :param copy_to: optionally copy the file to this destination
    :return:
    """
    existing_clusters = list_spark_clusters(config)

    kc = None
    for cluster in existing_clusters:
        if cluster['cluster_name'] == cluster_name:
            kc = oke.get_kube_config(config, cluster_id=cluster['cluster_id'])
            break

    if not kc:
        return None

    if copy_to:
        shutil.copy(src=kc.name, dst=copy_to)

    with open(kc.name) as f:
        return f.read()
