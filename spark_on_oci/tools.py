import sys, os
import ruamel.yaml
yaml = ruamel.yaml.YAML()

def get_package_dir():
    """Returns the directory that contains this package
        * If used as regular python module, this returns the parent of __file__
        * If used as executable, this returns the temporary directory used by the executable
    """
    if getattr(sys, 'frozen', False):
        # running in a frozen bundle
        parent_dir = sys._MEIPASS
    else:
        # running live
        parent_dir = os.path.dirname(__file__)
    return parent_dir

import collections
def dict_merge(dct, merge_dct):
    """ Recursive dict merge. Inspired by :meth:``dict.update()``, instead of
    updating only top-level keys, dict_merge recurses down into dicts nested
    to an arbitrary depth, updating keys. The ``merge_dct`` is merged into
    ``dct``.
    :param dct: dict onto which the merge is executed
    :param merge_dct: dct merged into dct
    :return: None
    """
    for k, v in merge_dct.items():
        if (k in dct and isinstance(dct[k], dict)
                and isinstance(merge_dct[k], collections.Mapping)):
            dict_merge(dct[k], merge_dct[k])
        else:
            dct[k] = merge_dct[k]

def yaml_dump(data,stream=sys.stdout):
    """Dump using ruamel to preserve round-trip"""
    return yaml.dump(data,stream)

def yaml_load(f):
    """Load using ruamel to preserve round-trip"""
    return yaml.load(f)

#############################
# Broken and/or Depricated.
# TODO: Remove these
# Kept here only for reference
#############################
NOTEBOOKS_BUCKET="jupyter-notebooks"
INDEXES_BUCKET="snap-indexes"
LOGS_BUCKET="snap-logs"
SNAP_SERVICE_VERSION="0.0.1"

def create_buckets(config,notebooks_bucket=NOTEBOOKS_BUCKET,indexes_bucket=INDEXES_BUCKET,logs_bucket=LOGS_BUCKET):
    
    bucket = indexes_bucket

    
    compartment_id = config["tenancy"]
    object_storage = oci.object_storage.ObjectStorageClient(config)
    namespace = object_storage.get_namespace().data
    # Create bucket for notebooks, if not exists

    def create_bucket(purpose,name):
        
        tags = {'snap-service-version': SNAP_SERVICE_VERSION,"snap-bucket-purpose":purpose}

        request = oci.object_storage.models.CreateBucketDetails(
            name=name,
            compartment_id=compartment_id,
            freeform_tags=tags
        )

        bucket_name_already_used = False
        try:
            bucket = object_storage.create_bucket(namespace, request).data
        except oci.exceptionsa.ServiceError as e:
            if e.code == "BucketAlreadyExists":
                bucket = object_storage.get_bucket(namespace_name=namespace,bucket_name=name).data
                if not bucket.freeform_tags == tags:
                    bucket_name_already_used = True
            else:
                raise e
        finally:
            if bucket_name_already_used:
                raise RuntimeError(f"Bucket {name} already exists ({bucket}), please choose a different bucket name for {purpose} or set the bucket's tags to {tags}")
    
        return bucket
    
    
    buckets = {
        NOTEBOOKS_BUCKET : create_bucket(NOTEBOOK_BUCKET,notebooks_bucket)
        ,INDEXES_BUCKET : create_bucket(INDEXES_BUCKET,indexes_bucket)
        ,LOGS_BUCKET : create_bucket(LOGS_BUCKET,logs_bucket)
    }
    
    return buckets
    
def create_spark_table_path(config, bucket_name, index_name):
    
    compartment_id = config["tenancy"]
    object_storage = oci.object_storage.ObjectStorageClient(config)
    namespace = object_storage.get_namespace().data
    object_name = index_name
    dummy_data = ""
    print(f"Creating object {bucket_name}@{namespace}/{index_name}")
    obj = object_storage.put_object(
        namespace,
        bucket_name,
        object_name,
        dummy_data).data
    
    return obj
    
def delete_vcn_by_tag(tag):
    structured_search = oci.resource_search.models.StructuredSearchDetails(query="query vcn resources where freeformTags.key = '{}'".format(tag),
                                                                           type='Structured',
                                                                           matching_context_type=oci.resource_search.models.SearchDetails.MATCHING_CONTEXT_TYPE_NONE)
    vcns = search_client.search_resources(structured_search)
    return vcns.data.items
    
    for v in vcns:
        print(v.display_name, v.identifier, v.lifecycle_state, v.time_created)
        try:
            oke.delete_vcn(clients[1],compartment_id=compartment_id,vcn_id=v.identifier)
        except Exception as e:
            print(e)


def create_helm_release(kubeconfig
                        , chart_values
                        , name
                        , chart_name
                        , chart_repo
                        , chart_namespace
                        , wait=False
                        , recreate=True
                        , install=True):

    upgrade_tiller_version(kubeconfig)

    local_repo = repo.from_repo(chart=chart_name, repo_url=chart_repo)
    chart = chartbuilder.ChartBuilder({'name': chart_name, 'source': {'type': 'directory', 'location': local_repo}})

    # Wait for tiller to be ready
    max_retries = 60
    retries = 0
    timeout = 30
    # TODO: Use kubernetes "watch" functionality get status
    tiller_pod_name, tiller_pod = GetTillerPod(kubeconfig)
    while tiller_pod.status.phase == "Pending":
        retries = retries + 1
        if retries > max_retries:
            raise RuntimeError(f"Could not create Helm release {name} from chart {chart_name}")
        print("Waiting {}s for Executors to be ready".format(timeout))
        time.sleep(timeout)
        tiller_pod_name, tiller_pod = GetTillerPod(kubeconfig)

    with tiller_proxy(kubeconfig=kubeconfig) as t:
        return t.update_release(chart.get_helm_chart()
                                 , namespace=chart_namespace
                                 , name=name
                                 , dry_run=False
                                 , values=chart_values
                                , recreate=recreate
                                , wait=wait
                                , install=install
                                , force=True)


###################################
# Helm and Tiller
###################################

import contextlib
#from pyhelm import chartbuilder, repo, tiller
import sys, time, os, subprocess, threading

def GetTillerPod(kubeconfig_file):
    kcconfig.load_kube_config(config_file=kubeconfig_file)
    v1 = kclient.CoreV1Api()
    pod_list = v1.list_namespaced_pod("kube-system")
    for pod in pod_list.items:
        if 'app' in pod.metadata.labels:
            if 'name' in pod.metadata.labels:
                if pod.metadata.labels['name'] == 'tiller':
                    return pod.metadata.name, pod
    return "", ""



@contextlib.contextmanager
def tiller_proxy(kubeconfig="", localhost="localhost", local_port=44134, remote_port=44134, logfile=None, *args, **kwds):
    if kubeconfig:
        os.environ["KUBECONFIG"] = kubeconfig

    tiller_pod_name, tiller_pod = GetTillerPod(kubeconfig)
    if logfile:
        print("create port-forwarding to Tiller pod {}".format(tiller_pod), file=logfile)

    command=['kubectl'
            , 'port-forward'
            , '--namespace'
            , 'kube-system'
            , 'pod/{}'.format(tiller_pod_name)
            , '{}:{}'.format(local_port, remote_port)]

    if logfile:
        print("proxy command: {}".format(" ".join(command)), file=logfile)

    proc = subprocess.Popen(command
                            , stdout=subprocess.PIPE
                            , stderr=subprocess.STDOUT)

    # DO NOT REMOVE THE SLEEP! (no, really)
    time.sleep(10)

    if logfile:
        def output_reader(proc):
            for line in iter(proc.stdout.readline, b''):
                print('[kubectl]: {0}'.format(line.decode('utf-8')), end='', file=logfile)

        t = threading.Thread(target=output_reader, args=(proc,))
        t.start()

    try:
        yield tiller.Tiller(localhost, local_port)
    except Exception as e:
        print(e, file=sys.stderr)
    finally:
        # Code to release resource, e.g.:
        proc.terminate()


def uninstall_helm_release(kubeconfig, name):

    with tiller_proxy(kubeconfig=kubeconfig) as t:
        return t.uninstall_release(name)


def list_helm_releases(kubeconfig):

    with tiller_proxy(kubeconfig=kubeconfig) as t:
        return t.list_releases()


def update_helm_release(config, cluster_id, chart_values: dict, name, chart_name, chart_repo, chart_namespace):

    kc = get_kube_config(config, cluster_id)

    local_repo = repo.from_repo(chart=chart_name, repo_url=chart_repo)
    chart = chartbuilder.ChartBuilder({'name': chart_name, 'source': {'type': 'directory', 'location': local_repo}})

    with tiller_proxy(kubeconfig=kc.name) as t:
        res = t.update_release(chart.get_helm_chart()
                         , dry_run=False
                         , name = name
                         , namespace= chart_namespace
                         , values=chart_values)

    return res
