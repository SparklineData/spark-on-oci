import abc
import logging
import builtins
import os
import re
import yaml
import json
import enum

import time
from kubernetes import client
from kubernetes.client.rest import ApiException
from kubernetes.client import models
import kubernetes

log = logging.getLogger('kubernetes_objects')



class ApiObject(abc.ABC):
    """ApiObject is the base class for many of the kubetest objects
    which wrap Kubernetes API objects.

    This base class provides common functionality and common object
    properties for all API wrappers. It also defines the following
    abstract methods which all subclasses must implement:

      - ``create``: create the resource on the cluster
      - ``delete``: remove the resource from the cluster
      - ``refresh``: refresh the underlying object model
      - ``is_ready``: check if the object is in the ready state

    Args:
         api_object: The underlying Kubernetes API object.

    Attributes:
        obj: The underlying Kubernetes API object.
    """

    # The Kubernetes API object type. Each subclass should
    # define its own obj_type.
    obj_type = None
    '''The default Kubernetes API object type for the class.
    Each subclass should define its own ``obj_type``.
    '''

    api_clients = None
    '''A mapping of all the supported api clients for the API
    object type. Various resources can have multiple versions,
    e.g. "apps/v1", "apps/v1beta1", etc. The preferred version
    for each resource type should be defined under the "preferred"
    key. The preferred API client will be used when the apiVersion
    is not specified for the resource.
    '''

    def __init__(self, api_object):
        # The underlying Kubernetes Api Object
        self.obj = api_object

        # The api client for the object. This will be determined
        # by the apiVersion of the object's manifest.
        self._api_client = None

    @property
    def version(self):
        """str: The API version of the Kubernetes object (`obj.apiVersion``)."""
        return self.obj.api_version

    @property
    def name(self):
        """str: The name of the Kubernetes object (``obj.metadata.name``)."""
        return self.obj.metadata.name

    @name.setter
    def name(self, name):
        """Set the name of the Kubernetes objects (``obj.metadata.name``)."""
        self.obj.metadata.name = name

    @property
    def namespace(self):
        """The namespace of the Kubernetes object (``obj.metadata.namespace``)."""
        return self.obj.metadata.namespace

    @namespace.setter
    def namespace(self, name):
        """Set the namespace of the object, if it hasn't already been set.

        Raises:
            AttributeError: The namespace has already been set.
        """
        if self.obj.metadata.namespace is None:
            self.obj.metadata.namespace = name
        else:
            raise AttributeError('Cannot set namespace - object already has a namespace')

    @property
    def api_client(self):
        """The API client for the Kubernetes object. This is determined
        by the ``apiVersion`` of the object configuration.

        Raises:
            ValueError: The API version is not supported.
        """
        if self._api_client is None:
            c = self.api_clients.get(self.version)
            # If we didn't find the client in the api_clients dict, use the
            # preferred version.
            if c is None:
                c = self.api_clients.get('preferred')
                if c is None:
                    raise ValueError(
                        'unknown version specified and no preferred version '
                        'defined for resource ({})'.format(self.version)
                    )
                log.warning(
                    'unknown version ({}) for {} {}, falling back to preferred version: {}'
                    .format(self.version,self.__class__.__name__,self.name, c)
                )

            # If we did find it, initialize that client version.
            self._api_client = c()
        return self._api_client

    def wait_until_ready(self, timeout=None, interval=1, fail_on_api_error=False):
        """Wait until the resource is in the ready state.

        Args:
            timeout (int): The maximum time to wait, in seconds, for
                the resource to reach the ready state. If unspecified,
                this will wait indefinitely. If specified and the timeout
                is met or exceeded, a TimeoutError will be raised.
            interval (int|float): The time, in seconds, to wait before
                re-checking if the object is ready.
            fail_on_api_error (bool): Fail if an API error is raised. An
                API error can be raised for a number of reasons, such as
                'resource not found', which could be the case when a resource
                is just being started or restarted. When waiting for readiness
                we generally do not want to fail on these conditions.
                (default: False)

        Raises:
             TimeoutError: The specified timeout was exceeded.
        """
        ready_condition = Condition(
            'api object ready',
            self.is_ready,
        )

        wait_for_condition(
            condition=ready_condition,
            timeout=timeout,
            interval=interval,
            fail_on_api_error=fail_on_api_error,
        )

    def wait_until_deleted(self, timeout=None, interval=1):
        """Wait until the resource is deleted from the cluster.

        Args:
            timeout (int): The maximum time to wait, in seconds, for
                the resource to be deleted from the cluster. If
                unspecified, this will wait indefinitely. If specified
                and the timeout is met or exceeded, a TimeoutError will
                be raised.
            interval (int|float): The time, in seconds, to wait before
                re-checking if the object has been deleted.

        Raises:
            TimeoutError: The specified timeout was exceeded.
        """
        def deleted_fn():
            try:
                self.refresh()
            except ApiException as e:
                # If we can no longer find the deployment, it is deleted.
                # If we get any other exception, raise it.
                if e.status == 404 and e.reason == 'Not Found':
                    return True
                else:
                    log.error('error refreshing object state')
                    raise e
            else:
                # The object was still found, so it has not been deleted
                return False

        delete_condition = Condition(
            'api object deleted',
            deleted_fn
        )

        wait_for_condition(
            condition=delete_condition,
            timeout=timeout,
            interval=interval,
        )

    @classmethod
    def load(cls, path):
        """Load the Kubernetes resource from file.

        Generally, this is used to load the Kubernetes manifest files
        and parse them into their appropriate API Object type.

        Args:
            path (str): The path to the YAML config file (manifest)
                containing the configuration for the resource.

        Returns:
            ApiObject: The API object wrapper corresponding to the configuration
            loaded from manifest YAML file.
        """
        obj = load_file(path, cls.obj_type)
        return cls(obj)

    @abc.abstractmethod
    def create(self, namespace=None):
        """Create the underlying Kubernetes resource in the cluster
        under the given namespace.

        Args:
            namespace (str): The namespace to create the resource under.
                If no namespace is provided, it will use the instance's
                namespace member, which is set when the object is created
                via the kubetest client. (optional)
        """

    @abc.abstractmethod
    def delete(self, options=None):
        """Delete the underlying Kubernetes resource from the cluster.

        This method expects the resource to have been loaded or otherwise
        assigned a namespace already. If it has not, the namespace will need
        to be set manually.

        Args:
            options (client.V1DeleteOptions): Options for resource deletion.
        """

    @abc.abstractmethod
    def refresh(self):
        """Refresh the local state (``obj``) of the underlying Kubernetes resource."""

    @abc.abstractmethod
    def is_ready(self):
        """Check if the resource is in the ready state.

        It is up to the wrapper subclass to define what "ready" means for
        that particular resource.

        Returns:
            bool: True if in the ready state; False otherwise.
        """

###########################
# Utils
###########################

def new_namespace(test_name):
    """Create a new namespace for the given test name.

    Kubernetes namespace names follow a DNS-1123 label that consists
    of lower case alphanumeric characters or '-' and must start with
    an alphanumeric.

    The test name and current timestamp are formatted to comply to
    this spec and appended to the 'kubetest' prefix.

    Args:
        test_name (str): The name of the test case for the namespace.

    Returns:
        str: The namespace name.
    """
    return 'kubetest-{}-{}'.format(
        test_name.replace('_', '-').lower(),
        int(time.time())
    )


def selector_string(selectors):
    """Create a selector string from the given dictionary of selectors.

    Args:
        selectors (dict): The selectors to stringify.

    Returns:
        str: The selector string for the given dictionary.
    """
    return ','.join(['{}={}'.format(k, v) for k, v in selectors.items()])


def selector_kwargs(fields=None, labels=None):
    """Create a dictionary of kwargs for Kubernetes object selectors.

    Args:
        fields (dict[str, str]): A dictionary of fields used to restrict
            the returned collection of Objects to only those which match
            these field selectors. By default, no restricting is done.
        labels (dict[str, str]): A dictionary of labels used to restrict
            the returned collection of Objects to only those which match
            these label selectors. By default, no restricting is done.

    Returns:
        dict[str, str]: A dictionary that can be used as kwargs for
            many Kubernetes API calls for label and field selectors.
    """
    kwargs = {}
    if fields is not None:
        kwargs['field_selector'] = selector_string(fields)
    if labels is not None:
        kwargs['label_selector'] = selector_string(labels)

    return kwargs

###############################
# Conditions
###############################
def wait_for_condition(condition, timeout=None, interval=1, fail_on_api_error=True):
    """Wait for a condition to be met.

    Args:
        condition (condition.Condition): The Condition to wait for.
        timeout (int): The maximum time to wait, in seconds, for the
            condition to be met. If unspecified, this function will
            wait indefinitely. If specified and the timeout is met
            or exceeded, a TimeoutError will be raised.
        interval (int|float): The time, in seconds, to wait before
            re-checking the condition.
        fail_on_api_error (bool): Fail the condition checks if a Kubernetes
            API error is incurred. An API error can be raised for a number
            of reasons, including a Pod being restarted and temporarily
            unavailable. Disabling this will cause those errors to be
            ignored, allowing the check to continue until timeout or
            resolution. (default: True).

    Raises:
        TimeoutError: The specified timeout was exceeded.
    """
    log.info('waiting for condition: %s', condition)

    # define the maximum time to wait. once this is met, we should
    # stop waiting.
    max_time = None
    if timeout is not None:
        max_time = time.time() + timeout

    # start the wait block
    start = time.time()
    while True:
        if max_time and time.time() >= max_time:
            raise TimeoutError(
                'timed out ({}s) while waiting for condition {}'
                .format(timeout, condition)
            )

        # check if the condition is met and break out if it is
        try:
            if condition.check():
                break
        except ApiException as e:
            log.warning('got api exception while waiting: {}'.format(e))
            if fail_on_api_error:
                raise

        # if the condition is not met, sleep for the interval
        # to re-check later
        time.sleep(interval)

    end = time.time()
    log.info('wait completed (total=%fs) %s', end - start, condition)


######################################
# Handle manifests
######################################


def load_file(path, obj_type=None):
    """Load an individual Kubernetes manifest YAML file.

    Args:
        path (str): The fully qualified path to the file.
        obj_type: The type of the object to load. If not specified,
            this will attempt to auto-detect the type.

    Returns:
        The Kubernetes API object for the manifest file.
    """
    with open(path, 'r') as f:
        manifest = yaml.load(f)

    if obj_type is None:
        obj_type = get_type(manifest)
        if obj_type is None:
            raise ValueError(
                'Unable to determine object type for manifest: {}'.format(manifest)
            )

    return new_object(obj_type, manifest)


def load_path(path):
    """Load all of the Kubernetes YAML manifest files found in the
    specified directory path.

    Args:
        path (str): The path to the directory of manifest files.

    Returns:
        list: A list of all the Kubernetes objects loaded from
            manifest file.

    Raises:
        ValueError: The provided path is not a directory.
    """
    if not os.path.isdir(path):
        raise ValueError('{} is not a directory'.format(path))

    objs = []
    for f in os.listdir(path):
        if os.path.splitext(f)[1].lower() in ['.yaml', '.yml']:
            obj = load_file(os.path.join(path, f))
            objs.append(obj)
    return objs


def get_type(manifest):
    """Get the Kubernetes object type from the manifest kind and
    version.

    There is no easy way for determining the internal model that a
    manifest should use. What this tries to do is use the version
    info and the kind info to create a potential internal object
    name for the pair and look that up in the kubernetes package
    locals.

    Args:
        manifest (dict): The manifest file, loaded into a dictionary.

    Returns:
        object: The Kubernetes API object for the manifest.
        None: No Kubernetes API object type could be determined.

    Raises:
        ValueError: The manifest dictionary does not have a
            `version` or `kind` specified.
    """
    version = manifest.get('apiVersion')
    if version is None:
        raise ValueError('manifest has no "version" field specified')

    kind = manifest.get('kind')
    if kind is None:
        raise ValueError('manifest has no "kind" field specified')

    # create a map of the kubernetes client model locals where the key is the
    # lower cased name (so we don't have to mess with getting the capitalization
    # of components correct) and the value is the correctly cased name.
    lookup = {k.lower(): k for k in models.__dict__.keys()}

    # if the version has a '/' (e.g. apps/v1, extensions/v1beta1), remove it.

    # there are generally two possibilities - we include the version minus
    # the slash (e.g. extensions/v1beta1 -> extensionsv1beta1), or we do not
    # use the prefix (e.g. apps/v1 -> v1). By default we will always try with
    # the prefix first and only try the secondary check if the first check
    # yields nothing.
    possibilities = [
        # add the default case
        version.replace('/', '').replace('.', '') + kind
    ]

    # if the prefix exists, add the non-prefixed version as a secondary check
    if version.count('/') == 1:
        possibilities.append(version.split('/')[1] + kind)

    for to_check in possibilities:
        type_name = lookup.get(to_check.lower())
        if type_name is None:
            continue
        return models.__dict__.get(type_name)
    return None


def load_type(obj_type, path):
    """Load a Kubernetes YAML manifest file for the specified type.

    While Kubernetes manifests can contain multiple object definitions
    in a single file (delimited with the YAML separator '---'), this
    does not currently support those files. This function expects a
    single object definition in the specified manifest file.

    Args:
        path (str): The path the manifest YAML to load.
        obj_type: The Kubernetes API object type that the YAML
            contents should be loaded into.

    Returns:
        A Kubernetes API object populated with the YAML contents.

    Raises:
        FileNotFoundError: The specified file was not found.
    """
    with open(path, 'r') as f:
        manifest = yaml.load(f)

    return new_object(obj_type, manifest)


def new_object(root_type, config):
    """Create a new Kubernetes API object and recursively populate it with
    the provided manifest configuration.

    The recursive population utilizes the swagger_types and attribute_map
    members of the Kubernetes API object class to determine which config
    fields correspond to which input parameter, and to cast them to their
    expected type.

    This is all based on the premise that the Python Kubernetes client will
    continue to be based off of an auto-generated Swagger spec and that these
    fields will be available for all API objects.

    Args:
        root_type: The Kubernetes API object type that will be populated
            with the manifest configuration. This is expected to be known
            ahead of time by the caller.
        config: The manifest configuration for the API object.

    Returns:
        A Kubernetes API object recursively populated with the YAML contents.
    """

    # The arguments that will be passed to the root_type to create a new
    # recursively populated instance of that type.
    constructor_args = {}

    # The attribute map maps the argument name (e.g. api_version) to the name
    # of the corresponding configuration field (e.g. apiVersion). Iterate over
    # each of these to pick up all the possible configuration options from the
    # provided manifest.
    for k, v in root_type.attribute_map.items():
        cfg_value = config.get(v)
        if cfg_value is not None:

            # The config value matches an expected key in the attribute dict.
            # Now, we want to cast that config to the appropriate type based
            # on the contents of the 'swagger_types' dict.
            t = root_type.swagger_types[k]

            # There are two classes of types we will want to check against:
            # 'base types' (like: str, int, etc) and 'collection types'
            # (like: list, dict). Collection types can contain base types,
            # so we will want to apply the same base type checks to each
            # element within a collection type. First we will check for the
            # collection types. If it is neither, we assume that the type is
            # a base type.

            # Check if the type is a list of some other type.
            # This should match to something like: 'list[str]', where the
            # element type (in this case 'str') will be isolated as a group.
            list_match = re.match(r'^list\[(.*)\]$', t)
            if list_match is not None:
                element_type = list_match.group(1)
                list_value = [cast_value(i, element_type) for i in cfg_value]
                constructor_args[k] = list_value
                continue

            # Check if the type is a dict composed of other types.
            # This should match to something lint: 'dict(str, str)', where
            # the element types (in this case, both 'str') will be isolated
            # as separate groups.
            dict_match = re.match(r'^dict\((.*), (.*)\)$', t)
            if dict_match is not None:
                key_type = dict_match.group(1)
                val_type = dict_match.group(2)
                dict_value = {
                    cast_value(k, key_type): cast_value(v, val_type)
                    for k, v in cfg_value.items()
                }
                constructor_args[k] = dict_value
                continue

            # If it is not a collection type, it must be a base type.
            constructor_args[k] = cast_value(cfg_value, t)

    return root_type(**constructor_args)


def cast_value(value, t):
    """Cast the given value to the specified type.

    There are two general cases for possible casts:
      - A cast to a builtin type (int, str, etc.)
      - A cast to a Kubernetes object (V1ObjectMeta, etc)

    In either case, check to see if the specified type exists in the
    correct type pool. If so, cast to that type, otherwise fail.

    Args:
        value: The value to cast.
        t (str): The type to cast the value to. This can be a builtin
            type or a Kubernetes API object type.

    Returns:
        The value, casted to the appropriate type.

    Raises:
        ValueError: Unable to cast the value to the specified type.
        TypeError: Unable to cast the given value to the specified type.
        AttributeError: The value is an invalid Kubernetes type.
    """

    # The config value should be cast to a built-in type
    builtin_type = builtins.__dict__.get(t)
    if builtin_type == object:
        return value
    if builtin_type is not None:
        return builtin_type(value)

    # The config value should be cast to a Kubernetes type
    k_type = kubernetes.client.__dict__.get(t)
    if k_type is not None:
        return new_object(k_type, value)

    raise ValueError('Unable to determine cast type behavior: {}'.format(t))


############################
# Kubernetes Objects
##########


class Container(object):
    """Kubetest wrapper around a Kubernetes `Container`_ API Object.

    The actual ``kubernetes.client.V1Container`` instance that this
    wraps can be accessed via the ``obj`` instance member.

    This wrapper provides some convenient functionality around the
    API Object and provides some state management for the `Container`_.

    This wrapper does **NOT** subclass the ``objects.ApiObject`` like other
    object wrappers because it is not intended to be created or
    managed from manifest file. It is merely meant to wrap the
    Container spec for a Pod to make Container-targeted actions
    easier.

    .. _Container:
        https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#container-v1-core
    """

    def __init__(self, api_object, pod):
        self.obj = api_object
        self.pod = pod

    def __str__(self):
        return str(self.obj)

    def __repr__(self):
        return self.__str__()

    def get_restart_count(self):
        """Get the number of times the Container has been restarted.

        Returns:
            int: The number of times the Container has been restarted.
        """
        container_name = self.obj.name
        pod_status = self.pod.status()

        # If there are no container status, the container hasn't started
        # yet, so there cannot be any restarts.
        if pod_status.container_statuses is None:
            return 0

        for status in pod_status.container_statuses:
            if status.name == container_name:
                return status.restart_count

        raise RuntimeError(
            'Unable to determine container status for {}'.format(container_name)
        )

    def get_logs(self):
        """Get all the logs for the Container.

        Returns:
            str: The Container logs.
        """
        return client.CoreV1Api().read_namespaced_pod_log(
            name=self.pod.name,
            namespace=self.pod.namespace,
            container=self.obj.name,
        )

    def search_logs(self, *keyword):
        """Search for keywords/phrases in the Container's logs.

        Args:
            *keyword (str): Keywords to search for within the logs.

        Returns:
            bool: True if found; False otherwise.
        """
        logs = self.get_logs()

        for k in keyword:
            if logs.find(k) == -1:
                return False
        return True



class Pod(ApiObject):
    """Kubetest wrapper around a Kubernetes `Pod`_ API Object.

    The actual ``kubernetes.client.V1Pod`` instance that this
    wraps can be accessed via the ``obj`` instance member.

    This wrapper provides some convenient functionality around the
    API Object and provides some state management for the `Pod`_.

    .. _Pod:
        https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#pod-v1-core
    """

    obj_type = client.V1Pod

    api_clients = {
        'preferred': client.CoreV1Api,
        'v1': client.CoreV1Api,
    }

    def __str__(self):
        return str(self.obj)

    def __repr__(self):
        return self.__str__()

    @property
    def version(self):
        return "preferred"

    def create(self, namespace=None):
        """Create the Pod under the given namespace.

        Args:
            namespace (str): The namespace to create the Pod under.
                If the Pod was loaded via the kubetest client, the
                namespace will already be set, so it is not needed
                here. Otherwise, the namespace will need to be provided.
        """
        if namespace is None:
            namespace = self.namespace

        log.info('creating pod "%s" in namespace "%s"', self.name, self.namespace)
        log.debug('pod: %s', self.obj)

        self.obj = self.api_client.create_namespaced_pod(
            namespace=namespace,
            body=self.obj,
        )

    def delete(self, options = None):
        """Delete the Pod.

        This method expects the Pod to have been loaded or otherwise
        assigned a namespace already. If it has not, the namespace will
        need to be set manually.

        Args:
            options (client.V1DeleteOptions): Options for Pod deletion.

        Return:
            client.V1Status: The status of the delete operation.
        """
        if options is None:
            options = client.V1DeleteOptions()

        log.info('deleting pod "%s"', self.name)
        log.debug('delete options: %s', options)
        log.debug('pod: %s', self.obj)

        return self.api_client.delete_namespaced_pod(
            name=self.name,
            namespace=self.namespace,
            body=options,
        )

    def refresh(self):
        """Refresh the underlying Kubernetes Pod resource."""
        self.obj = self.api_client.read_namespaced_pod_status(
            name=self.name,
            namespace=self.namespace,
        )

    def is_ready(self):
        """Check if the Pod is in the ready state.

        Returns:
            bool: True if in the ready state; False otherwise.
        """
        self.refresh()

        # if there is no status, the pod is definitely not ready
        status = self.obj.status
        if status is None:
            return False

        # check the pod phase to make sure it is running. a pod in
        # the 'failed' or 'success' state will no longer be running,
        # so we only care if the pod is in the 'running' state.
        phase = status.phase
        if phase.lower() != 'running':
            return False

        for cond in status.conditions:
            # we only care about the condition type 'ready'
            if cond.type.lower() != 'ready':
                continue

            # check that the readiness condition is True
            return cond.status.lower() == 'true'

        # Catchall
        return False

    def status(self):
        """Get the status of the Pod.

        Returns:
            client.V1PodStatus: The status of the Pod.
        """
        # first, refresh the pod state to ensure latest status
        self.refresh()

        # return the status of the pod
        return self.obj.status

    def get_containers(self):
        """Get the Pod's containers.

        Returns:
            list[Container]: A list of containers that belong to the Pod.
        """
        log.info('getting containers for pod "%s"', self.name)
        self.refresh()

        return [Container(c, self) for c in self.obj.spec.containers]

    def get_container(self, name):
        """Get a container in the Pod by name.

        Args:
            name (str): The name of the Container.

        Returns:
            Container: The Pod's Container with the matching name. If
            no container with the given name is found, ``None`` is returned.
        """
        for c in self.get_containers():
            if c.obj.name == name:
                return c
        return None

    def get_restart_count(self):
        """Get the total number of Container restarts for the Pod.

        Returns:
            int: The total number of Container restarts.
        """
        container_statuses = self.status().container_statuses
        if container_statuses is None:
            return 0

        total = 0
        for container_status in container_statuses:
            total += container_status.restart_count

        return total

    def http_proxy_get(self, path, query_params=None):
        """Issue a GET request to a proxy for the Pod.

        Notes:
            This function does not use the kubernetes
            ``connect_get_namespaced_pod_proxy_with_path`` function because there
            appears to be lack of support for custom query parameters (as of
            the ``kubernetes==7.0.0`` package version). To bypass this, parts of
            the core functionality from the aforementioned function are used here with
            the modification of allowing user-defined query parameters to be
            passed along.

        Args:
            path (str): The URI path for the request.
            query_params (dict[str, str]): Any query parameters for
                the request. (default: None)

        Returns:
            response.Response: The response data.
        """
        c = client.CoreV1Api()

        if query_params is None:
            query_params = {}

        path_params = {
            'name': self.name,
            'namespace': self.namespace
        }
        header_params = {
            'Accept': c.api_client.select_header_accept(['*/*']),
            'Content-Type': c.api_client.select_header_content_type(['*/*'])
        }
        auth_settings = ['BearerToken']

        try:
            resp = Response(*c.api_client.call_api(
                '/api/v1/namespaces/{namespace}/pods/{name}/proxy/' + path, 'GET',
                path_params=path_params,
                query_params=query_params,
                header_params=header_params,
                body=None,
                post_params=[],
                files={},
                response_type='str',
                auth_settings=auth_settings,
                _return_http_data_only=False,  # we want all info, not just data
                _preload_content=True,
                _request_timeout=None,
                collection_formats={}
            ))
        except ApiException as e:
            # if the ApiException does not have a body or headers, that
            # means the raised exception did not get a response (even if
            # it were 404, 500, etc), so we want to continue to raise in
            # that case. if there is a body and headers, we will not raise
            # and just take the data out that we need from the exception.
            if e.body is None and e.headers is None:
                raise

            resp = Response(
                data=e.body,
                status=e.status,
                headers=e.headers,
            )

        return resp

    def containers_started(self):
        """Check if the Pod's Containers have all started.

        Returns:
            bool: True if all Containers have started; False otherwise.
        """
        # start the flag as true - we will check the state and set
        # this to False if any container is not yet running.
        containers_started = True

        status = self.status()
        if status.container_statuses is not None:
            for container_status in status.container_statuses:
                if container_status.state is not None:
                    if container_status.state.running is not None:
                        if container_status.state.running.started_at is not None:
                            # The container is started, so move on to check the
                            # next container
                            continue
                # If we get here, then the container has not started.
                containers_started = containers_started and False
                break

        return containers_started

    def wait_until_containers_start(self, timeout=None):
        """Wait until all containers in the Pod have started.

        This will wait for the images to be pulled and for the containers
        to be created and started. This will unblock once all Pod containers
        have been started.

        This is different than waiting until ready, since a container may
        not be ready immediately after it has been started.

        Args:
            timeout (int): The maximum time to wait, in seconds, for the
                Pod's containers to be started. If unspecified, this will
                wait indefinitely. If specified and the timeout is met or
                exceeded, a TimeoutError will be raised.

        Raises:
            TimeoutError: The specified timeout was exceeded.
        """
        wait_condition = Condition(
            'all pod containers started',
            self.containers_started,
        )

        wait_for_condition(
            condition=wait_condition,
            timeout=timeout,
            interval=1,
        )


class Deployment(ApiObject):
    """Kubetest wrapper around a Kubernetes `Deployment`_ API Object.

    The actual ``kubernetes.client.V1Deployment`` instance that this
    wraps can be accessed via the ``obj`` instance member.

    This wrapper provides some convenient functionality around the
    API Object and provides some state management for the `Deployment`_.

    .. _Deployment:
        https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#deployment-v1-apps
    """

    obj_type = client.V1Deployment

    api_clients = {
        'preferred': client.AppsV1Api,
        'apps/v1': client.AppsV1Api,
        'apps/v1beta1': client.AppsV1beta1Api,
        'apps/v1beta2': client.AppsV1beta2Api,
    }

    def __str__(self):
        return str(self.obj)

    def __repr__(self):
        return self.__str__()

    def create(self, namespace=None):
        """Create the Deployment under the given namespace.

        Args:
            namespace (str): The namespace to create the Deployment under.
                If the Deployment was loaded via the kubetest client, the
                namespace will already be set, so it is not needed here.
                Otherwise, the namespace will need to be provided.
        """
        if namespace is None:
            namespace = self.namespace

        log.info('creating deployment "%s" in namespace "%s"', self.name, self.namespace)
        log.debug('deployment: %s', self.obj)

        self.obj = self.api_client.create_namespaced_deployment(
            namespace=namespace,
            body=self.obj,
        )

    def delete(self, options=None):
        """Delete the Deployment.

        This method expects the Deployment to have been loaded or otherwise
        assigned a namespace already. If it has not, the namespace will need
        to be set manually.

        Args:
            options (client.V1DeleteOptions): Options for Deployment deletion.

        Returns:
            client.V1Status: The status of the delete operation.
        """
        if options is None:
            options = client.V1DeleteOptions()

        log.info('deleting deployment "%s"', self.name)
        log.debug('delete options: %s', options)
        log.debug('deployment: %s', self.obj)

        return self.api_client.delete_namespaced_deployment(
            name=self.name,
            namespace=self.namespace,
            body=options,
        )

    def refresh(self):
        """Refresh the underlying Kubernetes Deployment resource."""
        self.obj = self.api_client.read_namespaced_deployment_status(
            name=self.name,
            namespace=self.namespace,
        )

    def is_ready(self):
        """Check if the Deployment is in the ready state.

        Returns:
            bool: True if in the ready state; False otherwise.
        """
        try:
            self.refresh()
        except client.rest.ApiException as e:
            # If we can no longer find the deployment, it is not ready.
            # If we get any other exception, raise it.
            if e.status == 404 and e.reason == 'Not Found':
                return False
            else:
                log.error('error refreshing object state')
                raise e

        # if there is no status, the deployment is definitely not ready
        status = self.obj.status
        if status is None:
            return False

        # check the status for the number of total replicas and compare
        # it to the number of ready replicas. if the numbers are
        # equal, the deployment is ready; otherwise it is not ready.
        # TODO (etd) - we may want some logging in here eventually
        total = status.replicas
        ready = status.ready_replicas

        if total is None:
            return False

        return total == ready

    def status(self):
        """Get the status of the Deployment.

        Returns:
            client.V1DeploymentStatus: The status of the Deployment.
        """
        log.info('checking status of deployment "%s"', self.name)
        # first, refresh the deployment state to ensure the latest status
        self.refresh()

        # return the status from the deployment
        return self.obj.status

    def get_pods(self):
        """Get the pods for the Deployment.

        Returns:
            list[Pod]: A list of pods that belong to the deployment.
        """
        log.info('getting pods for deployment "%s"', self.name)
        pods = client.CoreV1Api().list_namespaced_pod(
            namespace=self.namespace,
            label_selector=selector_string(self.obj.spec.selector.match_labels),
        )
        pods = [Pod(p) for p in pods.items]
        log.debug('pods: %s', pods)
        return pods


class Job(ApiObject):
    """Kubetest wrapper around a Kubernetes `Job` API Object.

    The actual ``kubernetes.client.V1Deployment`` instance that this
    wraps can be accessed via the ``obj`` instance member.

    This wrapper provides some convenient functionality around the
    API Object and provides some state management for the `Deployment`_.

    .. _Deployment:
        https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#deployment-v1-apps
    """

    obj_type = client.V1Job

    api_clients = {
        'preferred': client.BatchV1Api,
        'batch/v1': client.BatchV1Api,
        'apps/v1beta1': client.BatchV1beta1Api,
        'apps/v2alpha1': client.BatchV2alpha1Api,
    }

    def __str__(self):
        return str(self.obj)

    def __repr__(self):
        return self.__str__()

    def create(self, namespace=None):
        """Create the Deployment under the given namespace.

        Args:
            namespace (str): The namespace to create the Deployment under.
                If the Deployment was loaded via the kubetest client, the
                namespace will already be set, so it is not needed here.
                Otherwise, the namespace will need to be provided.
        """
        if namespace is None:
            namespace = self.namespace

        log.info('creating job "%s" in namespace "%s"', self.name, self.namespace)
        log.debug('job: %s', self.obj)

        self.obj = self.api_client.create_namespaced_job(
            namespace=namespace,
            body=self.obj,
        )

    def delete(self, options=None):
        """Delete the Deployment.

        This method expects the Deployment to have been loaded or otherwise
        assigned a namespace already. If it has not, the namespace will need
        to be set manually.

        Args:
            options (client.V1DeleteOptions): Options for Deployment deletion.

        Returns:
            client.V1Status: The status of the delete operation.
        """
        if options is None:
            options = client.V1DeleteOptions()

        log.info('deleting job "%s"', self.name)
        log.debug('delete options: %s', options)
        log.debug('deployment: %s', self.obj)

        return self.api_client.delete_namespaced_job(
            name=self.name,
            namespace=self.namespace,
            body=options,
        )

    def refresh(self):
        """Refresh the underlying Kubernetes Deployment resource."""
        self.obj = self.api_client.read_namespaced_job_status(
            name=self.name,
            namespace=self.namespace,
        )

    def is_ready(self):
        """Check if the Deployment is in the ready state.

        Returns:
            bool: True if in the ready state; False otherwise.
        """
        try:
            self.refresh()
        except client.rest.ApiException as e:
            # If we can no longer find the deployment, it is not ready.
            # If we get any other exception, raise it.
            if e.status == 404 and e.reason == 'Not Found':
                return False
            else:
                log.error('error refreshing object state')
                raise e

        # if there is no status, the deployment is definitely not ready
        status = self.obj.status
        if status is None:
            return False

        # check the status for the number of total replicas and compare
        # it to the number of ready replicas. if the numbers are
        # equal, the deployment is ready; otherwise it is not ready.
        # TODO (etd) - we may want some logging in here eventually
        total = status.replicas
        ready = status.ready_replicas

        if total is None:
            return False

        return total == ready

    def status(self):
        """Get the status of the Deployment.

        Returns:
            client.V1DeploymentStatus: The status of the Deployment.
        """
        log.info('checking status of deployment "%s"', self.name)
        # first, refresh the deployment state to ensure the latest status
        self.refresh()

        # return the status from the deployment
        return self.obj.status

    def get_pods(self):
        """Get the pods for the Deployment.

        Returns:
            list[Pod]: A list of pods that belong to the deployment.
        """
        log.info('getting pods for deployment "%s"', self.name)
        pods = client.CoreV1Api().list_namespaced_pod(
            namespace=self.namespace,
            label_selector=selector_string(self.obj.spec.selector.match_labels),
        )
        pods = [Pod(p) for p in pods.items]
        log.debug('pods: %s', pods)
        return pods

class ClusterRoleBinding(ApiObject):
    """Kubetest wrapper around a Kubernetes `ClusterRoleBinding`_ API Object.

    The actual ``kubernetes.client.V1ClusterRoleBinding`` instance that this
    wraps can be accessed via the ``obj`` instance member.

    This wrapper provides some convenient functionality around the
    API Object and provides some state management for the `ClusterRoleBinding`_.

    .. _ClusterRoleBinding:
        https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#clusterrolebinding-v1-rbac-authorization-k8s-io
    """

    obj_type = client.V1ClusterRoleBinding

    api_clients = {
        'preferred': client.RbacAuthorizationV1Api,
        'rbac.authorization.k8s.io/v1': client.RbacAuthorizationV1Api,
        'rbac.authorization.k8s.io/v1alpha1': client.RbacAuthorizationV1alpha1Api,
        'rbac.authorization.k8s.io/v1beta1': client.RbacAuthorizationV1beta1Api,
    }

    def __str__(self):
        return str(self.obj)

    def __repr__(self):
        return self.__str__()

    def create(self, namespace=None):
        """Create the ClusterRoleBinding under the given namespace.

        Args:
            namespace (str): This argument is ignored for ClusterRoleBindings.
        """
        log.info('creating clusterrolebinding "%s" in namespace "%s"', self.name, self.namespace)  # noqa
        log.debug('clusterrolebinding: %s', self.obj)

        self.obj = self.api_client.create_cluster_role_binding(
            body=self.obj,
        )

    def delete(self, options=None):
        """Delete the ClusterRoleBinding.

        This method expects the ClusterRoleBinding to have been loaded or otherwise
        assigned a namespace already. If it has not, the namespace will need
        to be set manually.

        Args:
             options (client.V1DeleteOptions): Options for ClusterRoleBinding deletion.

        Returns:
            client.V1Status: The status of the delete operation.
        """
        if options is None:
            options = client.V1DeleteOptions()

        log.info('deleting clusterrolebinding "%s"', self.name)
        log.debug('delete options: %s', options)
        log.debug('clusterrolebinding: %s', self.obj)

        return self.api_client.delete_cluster_role_binding(
            name=self.name,
            body=options,
        )

    def refresh(self):
        """Refresh the underlying Kubernetes ClusterRoleBinding resource."""
        self.obj = self.api_client.read_cluster_role_binding(
            name=self.name
        )

    def is_ready(self):
        """Check if the ClusterRoleBinding is in the ready state.

        ClusterRoleBindings do not have a "status" field to check, so we
        will measure their readiness status by whether or not they exist
        on the cluster.

        Returns:
            bool: True if in the ready state; False otherwise.
        """
        try:
            self.refresh()
        except client.rest.ApiException as e:
            # If we can no longer find the deployment, it is not ready.
            # If we get any other exception, raise it.
            if e.status == 404 and e.reason == 'Not Found':
                return False
            else:
                log.error('error refreshing clusterrolebinding state')
                raise e
        else:
            return True


class ServiceAccount(ApiObject):
    """Kubetest wrapper around a Kubernetes `ServiceAccount`_ API Object.

    The actual ``kubernetes.client.V1ServiceAccount`` instance that this
    wraps can be accessed via the ``obj`` instance member.

    This wrapper provides some convenient functionality around the
    API Object and provides some state management for the `ServiceAccount`_.

    .. _ServiceAccount:
        https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#service-account
    """

    obj_type = client.V1ServiceAccount

    api_clients = {
        'preferred': client.CoreV1Api,
        'v1': client.CoreV1Api,
    }

    def __str__(self):
        return str(self.obj)

    def __repr__(self):
        return self.__str__()

    def create(self, namespace=None):
        """Create the ServiceAccount under the given namespace.

        Args:
            namespace (str): The namespace to create the ServiceAccount under.
                If the ServiceAccount was loaded via the kubetest client, the
                namespace will already be set, so it is not needed here.
                Otherwise, the namespace will need to be provided.
        """
        if namespace is None:
            namespace = self.namespace

        log.info('creating serviceaccount "%s" in namespace "%s"', self.name, self.namespace)  # noqa
        log.debug('serviceaccount: %s', self.obj)

        self.obj = self.api_client.create_namespaced_service_account(
            namespace=namespace,
            body=self.obj,
        )

    def delete(self, options=None):
        """Delete the ServiceAccount.

        This method expects the ServiceAccount to have been loaded or otherwise
        assigned a namespace already. If it has not, the namespace will need
        to be set manually.

        Args:
             options (client.V1DeleteOptions): Options for ServiceAccount deletion.

        Returns:
            client.V1Status: The status of the delete operation.
        """
        if options is None:
            options = client.V1DeleteOptions()

        log.info('deleting serviceaccount "%s"', self.name)
        log.debug('delete options: %s', options)
        log.debug('serviceaccount: %s', self.obj)

        return self.api_client.delete_namespaced_service_account(
            namespace=self.namespace,
            name=self.name,
            body=options,
        )

    def refresh(self):
        """Refresh the underlying Kubernetes ServiceAccount resource."""
        self.obj = self.api_client.read_namespaced_service_account(
            namespace=self.namespace,
            name=self.name,
        )

    def is_ready(self):
        """Check if the ServiceAccount is in the ready state.

        ServiceAccounts do not have a "status" field to check, so we
        will measure their readiness status by whether or not they exist
        on the cluster.

        Returns:
            bool: True if in the ready state; False otherwise.
        """
        try:
            self.refresh()
        except client.rest.ApiException as e:
            # If we can no longer find the deployment, it is not ready.
            # If we get any other exception, raise it.
            if e.status == 404 and e.reason == 'Not Found':
                return False
            else:
                log.error('error refreshing service-account state')
                raise e
        else:
            return True


class Service(ApiObject):
    """Kubetest wrapper around a Kubernetes `Service`_ API Object.
    The actual ``kubernetes.client.V1Service`` instance that this
    wraps can be accessed via the ``obj`` instance member.
    This wrapper provides some convenient functionality around the
    API Object and provides some state management for the `Service`_.
    .. _Service:
        https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#service-v1-core
    """

    obj_type = client.V1Service

    api_clients = {
        'preferred': client.CoreV1Api,
        'v1': client.CoreV1Api,
    }

    def __str__(self):
        return str(self.obj)

    def __repr__(self):
        return self.__str__()

    def create(self, namespace=None):
        """Create the Service under the given namespace.
        Args:
            namespace (str): The namespace to create the Service under.
                If the Service was loaded via the kubetest client, the
                namespace will already be set, so it is not needed here.
                Otherwise, the namespace will need to be provided.
        """
        if namespace is None:
            namespace = self.namespace

        log.info('creating service "%s" in namespace "%s"', self.name, self.namespace)
        log.debug('service: %s', self.obj)

        self.obj = self.api_client.create_namespaced_service(
            namespace=namespace,
            body=self.obj,
        )

    def delete(self, options):
        """Delete the Service.
        This method expects the Service to have been loaded or otherwise
        assigned a namespace already. If it has not, the namespace will need
        to be set manually.
        Args:
            options (client.V1DeleteOptions): Options for Service deletion.
        Returns:
            client.V1Status: The status of the delete operation.
        """
        if options is None:
            options = client.V1DeleteOptions()

        log.info('deleting service "%s"', self.name)
        log.debug('delete options: %s', options)
        log.debug('service: %s', self.obj)

        return self.api_client.delete_namespaced_service(
            name=self.name,
            namespace=self.namespace,
            body=options,
        )

    def refresh(self):
        """Refresh the underlying Kubernetes Service resource."""
        self.obj = self.api_client.read_namespaced_service(
            name=self.name,
            namespace=self.namespace,
        )

    def is_ready(self):
        """Check if the Service is in the ready state.
        The readiness state is not clearly available from the Service
        status, so to see whether or not the Service is ready this
        will check whether the endpoints of the Service are ready.
        This comes with the caveat that in order for a Service to
        have endpoints, there needs to be some backend hooked up to it.
        If there is no backend, the Service will never have endpoints,
        so this will never resolve to True.
        Returns:
            bool: True if in the ready state; False otherwise.
        """
        try:
            self.refresh()
        except client.rest.ApiException as e:
            # If we can no longer find the deployment, it is not ready.
            # If we get any other exception, raise it.
            if e.status == 404 and e.reason == 'Not Found':
                return False
            else:
                log.error('error refreshing service state')
                raise e
        else:
            return True

        # check the status. if there is no status, the service is
        # definitely not ready.
        if self.obj.status is None:
            return False

        endpoints = self.get_endpoints()

        # if the Service has no endpoints, its not ready.
        if len(endpoints) == 0:
            return False

        # get the service endpoints and check that they are all ready.
        for endpoint in endpoints:
            # if we have an endpoint, but there are no subsets, we
            # consider the endpoint to be not ready.
            if endpoint.subsets is None:
                return False

            for subset in endpoint.subsets:
                # if the endpoint has no addresses setup yet, its not ready
                if subset.addresses is None or len(subset.addresses) == 0:
                    return False

                # if there are still addresses that are not ready, the
                # service is not ready
                not_ready = subset.not_ready_addresses
                if not_ready is not None and len(not_ready) > 0:
                    return False

        # if we got here, then all endpoints are ready, so the service
        # must also be ready
        return True

    def status(self):
        """Get the status of the Service.
        Returns:
            client.V1ServiceStatus: The status of the Service.
        """
        log.info('checking status of service "%s"', self.name)
        # first, refresh the service state to ensure the latest status
        self.refresh()

        # return the status from the service
        return self.obj.status

    def get_endpoints(self):
        """Get the endpoints for the Service.
        This can be useful for checking internal IP addresses used
        in containers, e.g. for container auto-discovery.
        Returns:
            list[client.V1Endpoints]: A list of endpoints associated
            with the Service.
        """
        log.info('getting endpoints for service "%s"', self.name)
        endpoints = self.api_client.list_namespaced_endpoints(
            namespace=self.namespace,
        )

        svc_endpoints = []
        for endpoint in endpoints.items:
            # filter to include only the endpoints with the same
            # name as the service.
            if endpoint.metadata.name == self.name:
                svc_endpoints.append(endpoint)

        log.debug('endpoints: %s', svc_endpoints)
        return svc_endpoints

    def proxy_http_get(self, path):
        """Issue a GET request to proxy of a Service.
        Args:
            path (str): The URI path for the request.
        Returns:
            str: The response data
        """
        return client.CoreV1Api().connect_get_namespaced_service_proxy_with_path(
            name="{}:{}".format(self.name, self.obj.spec.ports[0].port),
            namespace=self.namespace,
            path=path,
        )

class Role(ApiObject):
    """Kubetest wrapper around a Kubernetes `Role`_ API Object.

    The actual ``kubernetes.client.V1Role`` instance that this
    wraps can be accessed via the ``obj`` instance member.

    This wrapper provides some convenient functionality around the
    API Object and provides some state management for the `Role`_.

    .. _Role:
        https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#role-v1-rbac-authorization-k8s-io
    """

    obj_type = client.V1Role

    api_clients = {
        'preferred': client.RbacAuthorizationV1Api,
        'rbac.authorization.k8s.io/v1': client.RbacAuthorizationV1Api,
        'rbac.authorization.k8s.io/v1alpha1': client.RbacAuthorizationV1alpha1Api,
        'rbac.authorization.k8s.io/v1beta1': client.RbacAuthorizationV1beta1Api,
    }

    def __str__(self):
        return str(self.obj)

    def __repr__(self):
        return self.__str__()

    def create(self, namespace=None):
        """Create the Role under the given namespace.

        Args:
            namespace (str): The namespace to create the Role under.
                If the Role was loaded via the kubetest client, the
                namespace will already be set, so it is not needed here.
                Otherwise, the namespace will need to be provided.
        """
        if namespace is None:
            namespace = self.namespace

        log.info('creating role "%s" in namespace "%s"', self.name, self.namespace)  # noqa
        log.debug('role: %s', self.obj)

        self.obj = self.api_client.create_namespaced_role(
            namespace=namespace,
            body=self.obj,
        )

    def delete(self, options=None):
        """Delete the Role.

        This method expects the Role to have been loaded or otherwise
        assigned a namespace already. If it has not, the namespace will need
        to be set manually.

        Args:
             options (client.V1DeleteOptions): Options for Role deletion.

        Returns:
            client.V1Status: The status of the delete operation.
        """
        if options is None:
            options = client.V1DeleteOptions()

        log.info('deleting role "%s"', self.name)
        log.debug('delete options: %s', options)
        log.debug('role: %s', self.obj)

        return self.api_client.delete_namespaced_role(
            namespace=self.namespace,
            name=self.name,
            body=options,
        )

    def refresh(self):
        """Refresh the underlying Kubernetes Role resource."""
        self.obj = self.api_client.read_namespaced_role(
            namespace=self.namespace,
            name=self.name,
        )

    def is_ready(self):
        """Check if the Role is in the ready state.

        Roles do not have a "status" field to check, so we
        will measure their readiness status by whether or not they exist
        on the cluster.

        Returns:
            bool: True if in the ready state; False otherwise.
        """
        try:
            self.refresh()
        except client.rest.ApiException as e:
            # If we can no longer find the deployment, it is not ready.
            # If we get any other exception, raise it.
            if e.status == 404 and e.reason == 'Not Found':
                return False
            else:
                log.error('error refreshing role state')
                raise e
        else:
            return True


class Namespace(ApiObject):
    """Kubetest wrapper around a Kubernetes `Namespace`_ API Object.

    The actual ``kubernetes.client.V1Namespace`` instance that this
    wraps can be accessed via the ``obj`` instance member.

    This wrapper provides some convenient functionality around the
    API Object and provides some state management for the `Namespace`_.

    .. _Namespace:
        https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#namespace-v1-core
    """

    obj_type = client.V1Namespace

    api_clients = {
        'preferred': client.CoreV1Api,
        'v1': client.CoreV1Api,
    }

    def __str__(self):
        return str(self.obj)

    def __repr__(self):
        return self.__str__()

    @classmethod
    def new(cls, name):
        """Create a new Namespace with object backing.

        Args:
            name (str): The name of the new Namespace.

        Returns:
            Namespace: A new Namespace instance.
        """
        return cls(client.V1Namespace(
            api_version="v1",
            metadata=client.V1ObjectMeta(
                name=name
            )
        ))

    def create(self, name=None):
        """Create the Namespace under the given name.

        Args:
            name (str): The name to create the Namespace under. If the
                name is not provided, it will be assumed to already be
                in the underlying object spec. If it is not, namespace
                operations will fail.
        """
        if name is not None:
            self.name = name

        log.info('creating namespace "%s"', self.name)
        log.debug('namespace: %s', self.obj)

        self.obj = self.api_client.create_namespace(
            body=self.obj,
        )

    def delete(self, options=None):
        """Delete the Namespace.

        Args:
             options (client.V1DeleteOptions): Options for Namespace deletion.

        Returns:
            client.V1Status: The status of the delete operation.
        """
        if options is None:
            options = client.V1DeleteOptions()

        log.info('deleting namespace "%s"', self.name)
        log.debug('delete options: %s', options)
        log.debug('namespace: %s', self.obj)

        return self.api_client.delete_namespace(
            name=self.name,
            body=options,
        )

    def refresh(self):
        """Refresh the underlying Kubernetes Namespace resource."""
        self.obj = self.api_client.read_namespace(
            name=self.name,
        )

    def is_ready(self):
        """Check if the Namespace is in the ready state.

        Returns:
            bool: True if in the ready state; False otherwise.
        """
        try:
            self.refresh()
        except client.rest.ApiException as e:
            # If we can no longer find the deployment, it is not ready.
            # If we get any other exception, raise it.
            if e.status == 404 and e.reason == 'Not Found':
                return False
            else:
                log.error('error refreshing customresourcedefinition state')
                raise e

        status = self.obj.status
        if status is None:
            return False

        return status.phase.lower() == 'active'


#####################################
# CustomResourceDefinition
#####################################
def set_conditions(self, conditions):
    if conditions is None:
        conditions = []
    self._conditions = conditions

class CustomResourceDefinition(ApiObject):
    """Kubetest wrapper around a Kubernetes `CustomResourceDefinition`_ API Object.

    The actual ``kubernetes.client.V1betaCustonResourceDefinition`` instance that this
    wraps can be accessed via the ``obj`` instance member.

    This wrapper provides some convenient functionality around the
    API Object and provides some state management for the `CustomResourceDefinition`_.

    .. CustomResourceDefinition:
        https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.11/#customresourcedefinition-v1beta1-apiextensions-k8s-io
    """
    obj_type = client.V1beta1CustomResourceDefinition

    api_clients = {
        'preferred': client.ApiextensionsV1beta1Api,
        'apiextensions.k8s.io/v1beta1': client.ApiextensionsV1beta1Api,
    }

    def __str__(self):
        return str(self.obj)

    def __repr__(self):
        return self.__str__()

    # needed during create due to this bug:
    # https://github.com/kubernetes-client/python/issues/376
    from kubernetes.client.models.v1beta1_custom_resource_definition_status import V1beta1CustomResourceDefinitionStatus as crds
    setattr(crds, "conditions", property(fget=crds.conditions.fget, fset=set_conditions))

    def create(self,namespace=None):
        """
        Create the CustomResourceDefinition
        :return: V1beta1CustomResourceDefinition
        """

        log.info(f'creating customresourcedefinition {self.name}')
        log.debug(f'customresourcedefinition: {self.obj}')

        self.obj = self.api_client.create_custom_resource_definition(body=self.obj)
        return self.obj



    def delete(self, options: client.V1DeleteOptions = None):
        """
        Delete the custom resource definition

        *Warning* The CustomResourceDefinition will not be deleted until
        all of the instances of the resource's 'Kind' field have been deleted.
        :param options: optional deltion parameters (client.V1DeleteOptions)
        :return: V1Status
        """
        if not options:
            options = client.V1DeleteOptions()

        log.info(f'deleting customresourcedefinition {self.name}')
        log.debug(f'delete options: {options}')
        log.debug(f'clusterresourcedefinition:  {self.obj}')

        crd = self.api_client.delete_custom_resource_definition(
            body=options, name=self.name
        )
        return crd


    def is_ready(self):
        try:
            self.refresh()
        except client.rest.ApiException as e:
            # If we can no longer find the deployment, it is not ready.
            # If we get any other exception, raise it.
            if e.status == 404 and e.reason == 'Not Found':
                return False
            else:
                log.error('error refreshing customresourcedefinition state')
                raise e
        else:
            if self.obj.status:
                if self.obj.status.conditions:
                    lastStatus = self.obj.status.conditions[-1]
                    if lastStatus.status == "True" and lastStatus.type == "Established":
                        return True

        return False

    def refresh(self):
        """Refresh the underlying Kubernetes ConfigMap resource."""
        self.obj = self.api_client.read_custom_resource_definition(name=self.name)

    def get(self):
        return self.api_client.read_custom_resource_definition(name=self.name)



class Policy(enum.Enum):
    """Condition checking policies.

    A Policy defines the behavior of how Conditions are checked.

      - **ONCE**: A condition only needs to be met once and the check
        will consider it met regardless of the state of any other
        conditions that may be checked alongside it. This is the
        default behavior.
      - **SIMULTANEOUS**: A condition needs to be met simultaneous to
        all other conditions that are being checked alongside it for
        the check to be successful.
    """

    ONCE = 1
    SIMULTANEOUS = 2


class Condition:
    """A Condition is a convenience wrapper around a function and its arguments
    which allows the function to be called at a later time.

    The function is called in the ``check`` method, which resolves the result to
    a boolean value, thus the condition function should return a boolean or
    something that ultimately resolves to a Truthy or Falsey value.

    Args:
        name (str): The name of the condition to make it easier to identify.
        fn: The condition function that will be checked.
        *args: Any arguments for the condition function.
        **kwargs: Any keyword arguments for the condition function.

    Attributes:
        name (str): The name of the Condition.
        fn (callable): The condition function that will be checked.
        args (tuple): Arguments for the checking function.
        kwargs (dict): Keyword arguments for the checking function.
        last_check(bool): Holds the state of the last condition check.

    Raises:
        ValueError: The given ``fn`` is not callable.
    """

    def __init__(self, name, fn, *args, **kwargs):
        if not callable(fn):
            raise ValueError('The Condition function must be callable')

        self.name = name
        self.fn = fn
        self.args = args
        self.kwargs = kwargs

        # last check holds the state of the last check.
        self.last_check = False

    def __str__(self):
        return '<Condition (name: {}, met: {})>'.format(
            self.name, self.last_check
        )

    def __repr__(self):
        return self.__str__()

    def check(self):
        """Check that the condition was met.

        Returns:
            bool: True if the condition was met; False otherwise.
        """
        self.last_check = bool(self.fn(*self.args, **self.kwargs))
        return self.last_check


def check_all(*args):
    """Check all the given Conditions.

    Args:
        *args (Condition): The Conditions to check.

    Returns:
        bool: True if all checks pass; False otherwise.
    """
    return all([cond.check() for cond in args])


def check_and_sort(*args):
    """Check all the given Conditions and sort them into 'met' and
    'unmet' buckets.

    Args:
        *args (Condition): The Conditions to check.

    Returns:
        tuple[list[Condition], list[Condition]]: The met and unmet
        condition buckets (in that order).
    """
    met, unmet = [], []

    for c in args:
        if c.check():
            met.append(c)
        else:
            unmet.append(c)

    return met, unmet


class Response:
    """Response is a wrapper around the Kubernetes API's response data
    when a request is proxied to a Kubernetes resource, like a Pod or
    Service.

    A proxied request will return:
    - The response data
    - The response headers
    - The response HTTP code

    All of these are wrapped by this class. Additional helpers are provided,
    such as casting the response data to JSON.

    Args:
        data: The response data from the proxied request.
        status: The response status code.
        headers: The response headers.
    """

    def __init__(self, data, status, headers):
        self.data = data
        self.status = status
        self.headers = headers

    def json(self):
        """Convert the response data to JSON.

        If the response data is not valid JSON, an error will be raised.

        Returns:
            dict: The JSON data.
        """
        # the response data comes back as a string formatted as a python
        # dictionary might be, where the inner quotes are single quotes
        # (') instead of the double quotes (") expected by JSON standard.
        # to remedy this, we just replace single quotes with double quotes.
        data = self.data.replace("'", '"')
        return json.loads(data)
