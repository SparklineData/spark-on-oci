
from spark_on_oci import tools
import os
from kubernetes import client
import yaml, logging
from spark_on_oci import kubernetes_objects


DEFAULT_NAMESPACE="sparkns"
DEFAULT_MANIFEST_DIR = os.path.join(tools.get_package_dir(), "deploy")
DEFAULT_CLUSTER_MANIFEST=os.path.join(DEFAULT_MANIFEST_DIR, "spark-cluster.yaml")

log = logging.getLogger('spark_cluster')


###################################
# LoadBalancer - Service that allows access from external IP
###################################
class LoadBalancer(kubernetes_objects.Service):

    def __init__(self, namespace=DEFAULT_NAMESPACE):

        self.loadbalancer = kubernetes_objects.Service.load((
            os.path.join(DEFAULT_MANIFEST_DIR, "loadbalancer.yaml")
        ))
        super().__init__(api_object=self.loadbalancer.obj)

      #  if namespace:
      #      self.ns = kubernetes_objects.Namespace.new(namespace)
      #      self.namespace = namespace


###################################
# SparkOperator - Deployment that controls lifecycle of SparkCluster CustomResourceDefinition
###################################
class SparkOperator(kubernetes_objects.Deployment):

    def __init__(self, namespace=DEFAULT_NAMESPACE):

        #######################################
        # resources that must
        # be created before spark-operator
        #######################################
        self.deployment = kubernetes_objects.Deployment.load(
            os.path.join(DEFAULT_MANIFEST_DIR, "operator.yaml"))
        self.custom_resource_definition = kubernetes_objects.CustomResourceDefinition.load(
            os.path.join(DEFAULT_MANIFEST_DIR, "custom-resource.yaml"))
        self.cluster_role_binding = kubernetes_objects.ClusterRoleBinding.load(
            os.path.join(DEFAULT_MANIFEST_DIR, "cluster-admin-role.yaml"))
        self.service_account = kubernetes_objects.ServiceAccount.load(
            os.path.join(DEFAULT_MANIFEST_DIR, "service-account.yaml"))
        self.role = kubernetes_objects.Role.load(
            os.path.join(DEFAULT_MANIFEST_DIR, "role.yaml"))

        super().__init__(api_object=self.deployment.obj)
        if namespace:
            self.ns = kubernetes_objects.Namespace.new(namespace)
            self.namespace = namespace


    def create(self, ociuser=""):
        """
        Create the CustomResourceDefinition
        :return: V1beta1CustomResourceDefinition
        """

        if not self.custom_resource_definition.is_ready():

            self.custom_resource_definition.create()
            self.custom_resource_definition.wait_until_ready()

        if not self.cluster_role_binding.is_ready():
            if ociuser:
                self.cluster_role_binding.obj.subjects[-1].name = ociuser
            self.cluster_role_binding.obj.subjects[0].namespace = self.namespace
            self.cluster_role_binding.create()
            self.cluster_role_binding.wait_until_ready()

        if not self.ns.is_ready():
            self.ns.create()
            self.ns.wait_until_ready()

        self.service_account.namespace = self.namespace
        if not self.service_account.is_ready():
            self.service_account.create()
            self.service_account.wait_until_ready()

        self.role.namespace = self.namespace
        if not self.role.is_ready():
            self.role.create()
            self.role.wait_until_ready()

        log.info(f'creating customresourcedefinition {self.name}')
        log.debug(f'customresourcedefinition: {self.obj}')

        super().create(self.namespace)

    def delete(self):
        for r in [self.deployment, self.role, self.service_account, self.ns, self.cluster_role_binding, self.custom_resource_definition]:
            if not r.namespace:
                r.namespace = self.namespace
            if r.is_ready():
                r.delete()
                r.wait_until_deleted()

        super().delete()


class SparkCluster(kubernetes_objects.ApiObject):
    """Kubetest wrapper around a Kubernetes `CustomObject`_ API Object.

    The actual ``kubernetes.client.V1betaCustomObject`` instance that this
    wraps can be accessed via the ``obj`` instance member.

    This wrapper provides some convenient functionality around the
    API Object and provides some state management for the `CustomObject`_.

    .. CustomObject:
        https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.11/#customobject-v1beta1-apiextensions-k8s-io
    """

    _version = "v1alpha1"
    _group = "spark-on-oci.org"
    _plural = "sparkclusters"

    api_clients = {
        'preferred': client.CustomObjectsApi,
        f'{_group}/{_version}': client.CustomObjectsApi,
    }

    def __init__(self,namespace=DEFAULT_NAMESPACE,path=DEFAULT_CLUSTER_MANIFEST):
        obj = load_custom_resource(SparkCluster, path)
        super().__init__(api_object=obj)
        self.obj['metadata']['namespace'] = namespace

    @property
    def version(self):
        return self.obj['apiVersion']

    @property
    def name(self):
        return self.obj['metadata']['name']

    @property
    def namespace(self):
        return self.obj['metadata']['namespace']

    def create(self):
        """
        Create the CustomObject
        :return: V1beta1CustomObject
        """

        log.info(f'creating {self._plural} {self.name}')
        log.debug(f'{self._plural}: {self.obj}')

        self.obj = self.api_client.create_namespaced_custom_object(body=self.obj,
                                                        namespace=self.namespace,
                                                        version=self._version,
                                                        group=self._group,
                                                        plural=self._plural)

        return self.obj


    def pods(self):

        log.info('listing pods "%s" in SparkCluster namespace "%s"', self.name, self.namespace)
        log.debug('SparkCluster: %s', self.obj)

        pods = client.CoreV1Api().list_namespaced_pod(
            namespace=self.namespace,
            label_selector='',
        )
        pods = [kubernetes_objects.Pod(p) for p in pods.items]
        return pods

    def all_logs(self,pod_name):
        pods = self.pods()
        for p in pods:
            if pod_name in p.name:
                containers = p.get_containers()
                for c in containers:
                    yield {'pod': p.name, 'container': c.obj.name, 'logs': c.get_logs()}

    def notebook_logs(self,pprint=True):
        logs = list(self.all_logs('notebook'))
        if not pprint:
            return logs
        return self._pprint_logs(logs)

    def thriftserver_logs(self,pprint=True):
        logs = list(self.all_logs('thriftserver'))
        if not pprint:
            return logs
        return self._pprint_logs(logs)

    def _pprint_logs(self,logs):
        for log in logs:
            print(f"Pod: {log['pod']}")
            print(f"  Container: {log['container']}")
            print(f"    Logs: \n {log['logs']}")



    def delete(self, options: client.V1DeleteOptions = None):
        """
        Delete the custom resource definition

        *Warning* The CustomResourceDefinition will not be deleted until
        all of the instances of the resource's 'Kind' field have been deleted.
        :param options: optional deltion parameters (client.V1DeleteOptions)
        :return: V1Status
        """
        if not options:
            options = client.V1DeleteOptions()

        log.info(f'deleting {self._plural} {self.name}')
        log.debug(f'delete options: {options}')
        log.debug(f'{self._plural}:  {self.obj}')

        crd = self.api_client.delete_namespaced_custom_object(
            body=options, name=self.name,
            namespace=self.namespace, version=self._version,
            plural=self._plural,group=self._group
        )
        return crd


    def is_ready(self):
        try:
            self.refresh()
        except client.rest.ApiException as e:
            # If we can no longer find the deployment, it is not ready.
            # If we get any other exception, raise it.
            if e.status == 404 and e.reason == 'Not Found':
                return False
            else:
                log.error(f'error refreshing {self._plural} {self.name} state')
                raise e
        else:
            return True

    def refresh(self):
        """Refresh the underlying Kubernetes custom object resource."""
        self.obj = self.api_client.get_namespaced_custom_object(self._group,
                                                                self._version,
                                                                self.namespace,
                                                                self._plural,
                                                                self.name
                                                                )

    def get(self):
        return self.api_client.read_custom_resource_definition(name=self.name)


    @classmethod
    def load(cls, path):

        api_object = load_custom_resource(SparkCluster,path)
        return cls(api_object=api_object)


def load_custom_resource(cls,path):

    with open(path, 'r') as f:
        manifest = tools.yaml_load(f)

    apiVersion = manifest.get("apiVersion")
    cls._group, cls._version = apiVersion.split("/")
    kind = manifest.get("kind")
    assert apiVersion in cls.api_clients
    assert kind == cls.__name__
    manifest['metadata']['namespace'] = None

    return manifest
