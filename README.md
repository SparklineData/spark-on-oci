# Spark-on-OCI

This repository provides a commandline tools, python modules, notebooks, helm charts, and documentation
for running Apache Spark on Oracle Clound Infrastructure.  [See the detailed documentation](https://sparklinedata.gitlab.io/spark-on-oci/)


# Using the Command Line Tool

### Requirements
* Python 3.6, Pip
* Follow the steps to [Create OCI Configuration File](https://sparklinedata.gitlab.io/spark-on-oci/Getting-Started/Creating-the-OCI-config-file/create-oci-config/)

### Installation

```bash
pip install git+https://gitlab.com/SparklineData/spark-on-oci
```

### Usage

To see help for each command, run:

```bash
spark-on-oci <section> <command> -- --help
```

List existing clusters:

```bash
spark-on-oci cluster status
```

## 1. Creating a Cluster:

The command to create a cluster with default configurations is:

```bash
spark-on-oci cluster create --cluster-name spark-2

+---------+-----------------+----------------+-----------------------------------------------------+
|   Name  | Workers (total) |     Shape      |                        Zones                        |
+---------+-----------------+----------------+-----------------------------------------------------+
| spark-2 |        6        | VM.Standard1.4 | ['xlLj:PHX-AD-1', 'xlLj:PHX-AD-2', 'xlLj:PHX-AD-3'] |
+---------+-----------------+----------------+-----------------------------------------------------+
```

The default cluster name is `spark-on-oci`.  You can change it by:
* providing the `--cluster-name` flag with each command
* setting the `OCI_CLUSTER_NAME` environment variable

### Choosing availability zones:

By default, all availability zones in the tenancy will be used.  You can restrict
the number of zones (must be 2 or more) with the `--zones` flag:

```
$ spark-on-oci cluster zones

+---------------+---------------------------------------------------
|      Name     |                   OCID                           |
+---------------+--------------------------------------------------+
| xlLj:PHX-AD-1 | ocid1.availabilitydomain.oc1..aaaaaaaakemzo...   |
| xlLj:PHX-AD-2 | ocid1.availabilitydomain.oc1..aaaaaaaac7uyd...   |
| xlLj:PHX-AD-3 | ocid1.availabilitydomain.oc1..aaaaaaaaupx7...    |
+---------------+--------------------------------------------------+

$ spark-on-oci cluster create --cluster-name myspark --zones '["xlLj:PHX-AD-1","xlLj:PHX-AD-2"]'
```

### Choosing worker shapes

You can select the available shapes for Spark workers with the `--shape` flag:

```bash
$ spark-on-oci cluster shapes

+----------------+-----+----------+----------+
|      Name      | CPU | MEM (gb) | DENSE IO |
+----------------+-----+----------+----------+
| VM.Standard1.4 |  4  |    28    |  False   |
| VM.Standard1.8 |  8  |    56    |  False   |
| VM.Standard2.2 |  2  |    30    |  False   |
| VM.DenseIO1.4  |  4  |    60    |   True   |
| VM.DenseIO1.8  |  8  |   120    |   True   |
+----------------+-----+----------+----------+

$ spark-on-oci cluster create --shape VM.DenseIO1.4
```

**Warning:** The shapes listed are the ones supported by `spark-on-oci` and may
not be available in your tenancy.

### Choose number of workers

You can choose the number of Spark worker nodes with the `--workers-per-zone` flag.

For example, this command will create a cluster with 2 availability zones, 4 workers in each zone for a total of 8 workers

```bash
$ spark-on-oci cluster create --cluster-name myspark --zones '["xlLj:PHX-AD-1","xlLj:PHX-AD-2"]' --workers-per-zone 4
```

### Accessing the cluster using Kubernetes tools

**Advanced:** If you want to access the cluster using `kubectl` or `helm` tool, 
use the `get-kubeconf` command to download the kubernetes 
configuration into your `~/.kube/config` file

```bash
spark-on-oci cluster get-kubeconf --cluster-name myspark
```

## 2. Start Spark

Once the cluster is created, you can start Spark with a default configuration 
using the `spark start` command:

```bash
spark-on-oci spark start --cluster-name myspark
```

### Basic HTTP Authentication:

By default all endpoints are publicly accessible.  To add authentication, create an auth file using the `htpasswd` tool.

```bash
htpasswd -c ./auth <username>
# ... you will be prompted for password twice ...
``` 

Now use the contents of the auth file with the `--auth` flag

```bash
spark-on-oci spark start --cluster-name myspark --auth "$(cat auth)"
```

Now all urls given by the `spark urls` command should require authentication.

### Choosing Spark Version:

By default, Spark 2.3.1 is used on all nodes.  

Supported Spark Versions are:

* Spark 2.2.1
* Spark 2.3.1
* Spark 2.4.3

You can use the `SPARK_VERSION` environment variable or the `--spark-version` flag

```
spark-on-oci spark generate-config --cluster-name myspark --spark-version 2.2.1
```

#### Modifying the Spark configuration.

Use the `spark generate-config` to generate the default configuration file for a cluster.

The values in this file will depend on the shapes of the nodes in the cluster. You
can edit this file to modify the default configuration and Spark properties.

```bash
spark-on-oci spark generate-config --cluster-name myspark --output-file myspark-config.yaml
```

After editing this file, recreate all the Spark components with the new configuration:

```bash
spark-on-oci spark start --cluster-name myspark --sparkcluster-yaml myspark-config.yaml --recreate
```


## 3. Check Spark Status

```
spark-on-oci spark status

+---------------------------------------------------------------+-------+----------+
|                           Component                           | Ready | Restarts |
+---------------------------------------------------------------+-------+----------+
|    spark-7us3k97hvxup8d0rb1z1yw03b-master-574884445c-55565    |  True |    0     |
|   spark-7us3k97hvxup8d0rb1z1yw03b-notebook-55df495769-hcn4v   |  True |    0     |
| spark-7us3k97hvxup8d0rb1z1yw03b-thriftserver-694846d6b4-pz97x |  True |    0     |
|            spark-7us3k97hvxup8d0rb1z1yw03b-worker-0           |  True |    0     |
|            spark-7us3k97hvxup8d0rb1z1yw03b-worker-1           |  True |    0     |
|            spark-7us3k97hvxup8d0rb1z1yw03b-worker-2           |  True |    0     |
|            spark-7us3k97hvxup8d0rb1z1yw03b-worker-3           |  True |    0     |
|            spark-7us3k97hvxup8d0rb1z1yw03b-worker-4           |  True |    0     |
|            spark-7us3k97hvxup8d0rb1z1yw03b-worker-5           |  True |    0     |
|                spark-operator-548bc9d88f-2pkjm                |  True |    0     |
+---------------------------------------------------------------+-------+----------+

```

## 4. Access Spark Endpoints

Use the `spark urls` command to see the URLs for secure accessible endpoints (Spark UI, notebook, jdbc etc)

```bash
spark-on-oci spark urls --cluster-name spark-on-oci

External urls for cluster spark-on-oci
+------------------------+----------------+
|          URL           |      Name      |
+------------------------+----------------+
|    129.213.210.89/     |   forecastle   |
|  129.213.210.89/jdbc   |      jdbc      |
| 129.213.210.89/jupyter |    notebook    |
|  129.213.210.89/spark  | spark-ui-proxy |
+------------------------+----------------+
```

**[DEPRECATED]** Use the `spark endpoints` command to see the URLs for externally accessible endpoints (Spark UI, notebook, jdbc etc)

```
spark-on-oci spark endpoints --cluster-name myspark

+-----------------------+-------------------------------------------------+
|          URL          |                       Name                      |
+-----------------------+-------------------------------------------------+
| 129.1.....120:30561   |    spark-7us3k97hvxup8d0rb1z1yw03b-master-ui    |
|  129......74:31288    |     spark-7us3k97hvxup8d0rb1z1yw03b-notebook    |
| 129......176:31534    | spark-7us3k97hvxup8d0rb1z1yw03b-thriftserver-ui |
+-----------------------+-------------------------------------------------+
```

#### Access JDBC with `pyhive`

The Spark ThriftServer JDBC endpoint is running over HTTP mode by default.

See the `examples/pyhive/README.md` file for an example of how to connect to the JDBC endpoint with pyhive.

#### Access Spark Notebook

Use the `notebook token` command to see the get the token you will need to login to the notebook

```
spark-on-oci notebook token --cluster-name myspark

```


## 5. Stop Spark

To delete all the Spark components without shutting down the hardware cluster:

```bash
spark-on-oci spark stop --cluster-name myspark
```

## 6. Deleting the Cluster

To permenently delete all of the hardware for the cluster:

```bash
spark-on-oci cluster delete --cluster-name myspark --force
```

Use the `--force` to force the deletion in case of any error.

# Starting the Spark-on-OCI Controller notebook

```bash
curl -o spark-controller.sh https://gitlab.com/SparklineData/spark-on-oci/raw/master/bin/spark-controller.sh && chmod a+x spark-controller.sh
./spark-controller.sh
```

[See the Documentation](https://sparklinedata.gitlab.io/spark-on-oci/)

## Updating the Documentation

This project uses **[`mkdocs`](https://www.mkdocs.org/)** for documentation.

You can use the `mkdocs serve` development server to help with adding more documentation.
To install the mkdocs requirements run:

```bash
pip install -r requirements-mkdocs.txt
mkdocs serve
```

This will install mkdocs and other requirements needed to build the documentation.

Now you can run the development server from the root directory of this repository:

```bash
mkdocs serve
```

This should start a server accessible at http://127.0.0.1:8000.  You can edit the documentation files
and see live updates to the documentation.
