# Running SNAP on OCI

## Creating the configuration file

1. Create a SNAP properties file (`snap.properties`) or use an existing one.

2. Create a new `snapcluster.yaml` file with the following contents. Change the `SNAP_DOWNLOAD_URL` to your link to the SNAP download

```yaml
spec:

  Master:
    ImageTag: 2.2.1

  Worker:
    ImageTag: 2.2.1

  SparkApplication:
    ImagePullPolicy: Always
    ImageTag: 2.2.1
    ServiceType: "NodePort"
    InstallCommand: "wget -qO- 'SNAP_DOWNLOAD_URL' | tar xvz -C /app --strip-components=1"
    StartCommand: "/app/bin/snap-tool start"
    Env:
      SNAP_PROPERTIES: "/conf/spark.properties"
```

## Starting SNAP

Use the `spark-on-oci spark start` command to start SNAP on the cluster.

```bash
spark-on-oci spark start --cluster-name spark-on-oci --sparkcluster-yaml snapcluster.yaml --spark-properties snap.properties
```

SNAP should now be running as the thriftserver pod.