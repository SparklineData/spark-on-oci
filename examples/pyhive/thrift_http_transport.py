import base64
from thrift.transport.TTransport import TTransportBase
from io import BytesIO
import os
import ssl
import sys
from six.moves import urllib
from six.moves import http_client
import six


def http_mode(uri, username="x", password="x",
              cafile=None,
              cadata=None,
              cert_file=None,
              key_file=None,
              ssl_context=None,
              verify=True,
              cookie_auth=False):
    """
    Create a http mode transport

    :param host: uri in form of http(s)://<jdbc-host>/some/path
    :param username: user name
    :param password: password
    :param cafile: path to .cert file to connect to SSL endpoint. Required if using SSL
    :param cert_file:
    :param key_file:
    :param ssl_context:
    :return: the TTransportBase with HTTPS enabled
    """
    if not verify:
        ssl.create_default_context = ssl._create_unverified_context

    ap = bytes(f"{username}:{password}", 'UTF-8')
    pwbase64 = base64.b64encode(ap).decode("ascii")

    _transport = THttpClient(uri
                             , cafile=cafile
                             , cadata=cadata
                             , cert_file=cert_file
                             , key_file=key_file
                             , ssl_context=ssl_context
                             , cookie_auth=cookie_auth)

    _transport.setCustomHeaders({"Authorization": f"Basic {pwbase64}"})
    return _transport

class THttpClient(TTransportBase):
    """Taken from the Http implementation of TTransport base."""

    def __init__(self, uri_or_host
                 , cafile=None
                 , cadata=None
                 , cert_file=None
                 , key_file=None
                 , ssl_context=None
                 , cookie_auth=False):
        """

        :param uri_or_host:
        :param cafile:
        :param cert_file:
        :param key_file:
        :param ssl_context:
        :param cookie_auth: respects 'Set-Cookie' header from server

        To properly authenticate against the server,
        provide the client's identity by specifying cert_file and key_file.  To properly
        authenticate the server, specify either cafile or ssl_context with a CA defined.
        NOTE: if ssl_context is defined, it will override cafile and cadata
        """

        parsed = urllib.parse.urlparse(uri_or_host)
        self.scheme = parsed.scheme
        assert self.scheme in ('http', 'https')
        if self.scheme == 'http':
            self.port = parsed.port or http_client.HTTP_PORT
        elif self.scheme == 'https':
            self.port = parsed.port or http_client.HTTPS_PORT
            self.certfile = cert_file
            self.keyfile = key_file

            if not cafile and not cadata:
                cadata = get_certificate(uri_or_host)

            self.context = ssl_context if ssl_context \
                else ssl.create_default_context(cadata=cadata, cafile=cafile)

        self.host = parsed.hostname
        self.path = parsed.path
        if parsed.query:
            self.path += '?%s' % parsed.query
        try:
            proxy = urllib.request.getproxies()[self.scheme]
        except KeyError:
            proxy = None
        else:
            if urllib.request.proxy_bypass(self.host):
                proxy = None
        if proxy:
            parsed = urllib.parse.urlparse(proxy)
            self.realhost = self.host
            self.realport = self.port
            self.host = parsed.hostname
            self.port = parsed.port
            self.proxy_auth = self.basic_proxy_auth_header(parsed)
        else:
            self.realhost = self.realport = self.proxy_auth = None
        self.__wbuf = BytesIO()
        self.__http = None
        self.__http_response = None
        self.__timeout = None
        self.__custom_headers = None
        self._set_cookie = cookie_auth

    @staticmethod
    def basic_proxy_auth_header(proxy):
        if proxy is None or not proxy.username:
            return None
        ap = "%s:%s" % (urllib.parse.unquote(proxy.username),
                        urllib.parse.unquote(proxy.password))
        cr = base64.b64encode(ap).strip()
        return "Basic " + cr

    def using_proxy(self):
        return self.realhost is not None

    def open(self):
        if self.scheme == 'http':
            self.__http = http_client.HTTPConnection(self.host, self.port,
                                                     timeout=self.__timeout)
        elif self.scheme == 'https':
            self.__http = http_client.HTTPSConnection(self.host, self.port,
                                                      key_file=self.keyfile,
                                                      cert_file=self.certfile,
                                                      timeout=self.__timeout,
                                                      context=self.context)
        if self.using_proxy():
            self.__http.set_tunnel(self.realhost, self.realport,
                                   {"Proxy-Authorization": self.proxy_auth})

    def close(self):
        self.__http.close()
        self.__http = None
        self.__http_response = None

    def isOpen(self):
        return self.__http is not None

    def setTimeout(self, ms):
        if ms is None:
            self.__timeout = None
        else:
            self.__timeout = ms / 1000.0

    def setCustomHeaders(self, headers):
        self.__custom_headers = headers

    def read(self, sz):
        return self.__http_response.read(sz)

    def write(self, buf):
        self.__wbuf.write(buf)

    def flush(self):
        if self.isOpen():
            self.close()
        self.open()

        # Pull data out of buffer
        data = self.__wbuf.getvalue()
        self.__wbuf = BytesIO()

        # HTTP request
        if self.using_proxy() and self.scheme == "http":
            # need full URL of real host for HTTP proxy here (HTTPS uses CONNECT tunnel)
            self.__http.putrequest('POST', "http://%s:%s%s" %
                                   (self.realhost, self.realport, self.path))
        else:
            self.__http.putrequest('POST', self.path)

        # Write headers
        self.__http.putheader('Content-Type', 'application/x-thrift')
        self.__http.putheader('Content-Length', str(len(data)))
        if self.using_proxy() and self.scheme == "http" and self.proxy_auth is not None:
            self.__http.putheader("Proxy-Authorization", self.proxy_auth)

        if not self.__custom_headers or 'User-Agent' not in self.__custom_headers:
            user_agent = 'Python/THttpClient'
            script = os.path.basename(sys.argv[0])
            if script:
                user_agent = '%s (%s)' % (user_agent, urllib.parse.quote(script))
            self.__http.putheader('User-Agent', user_agent)

        if self.__custom_headers:
            for key, val in six.iteritems(self.__custom_headers):
                self.__http.putheader(key, val)

        self.__http.endheaders()

        # Write payload
        self.__http.send(data)

        # Get reply to flush the request
        self.__http_response = self.__http.getresponse()
        self.code = self.__http_response.status
        self.message = self.__http_response.reason
        self.headers = self.__http_response.msg

        if self._set_cookie and 'Cookie' not in self.__custom_headers:
            for (hname, hval) in self.headers._headers:
                if hname == 'Set-Cookie':
                    self.__custom_headers['Cookie'] = hval
                    break

def get_certificate(url: str) -> str:
    """Download certificate from remote server.
    Args:
        url (str): url to get certificate from
    Returns:
        str: certificate string in PEM format
    """
    parsed_url = urllib.parse.urlparse(url)
    import socket
    hostname = parsed_url.hostname
    port = int(parsed_url.port or 443)
    conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    context = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
    sock = context.wrap_socket(conn, server_hostname=hostname)
    sock.connect((hostname, port))
    return ssl.DER_cert_to_PEM_cert(sock.getpeercert(True))

if __name__ == '__main__':
    from pyhive import hive
    import sys
    url = sys.argv[1]
    username = sys.argv[2]
    pw = sys.argv[3]
    conn = hive.connect(thrift_transport=http_mode(uri=url
                                                   ,username=username
                                                   ,password=pw
                                                   ,verify=False
                                                   ,cookie_auth=True))
    cursor = conn.cursor()
    cursor.execute("show databases")
    print(cursor.fetchone())
    cursor.execute("show databases")
    print(cursor.fetchone())
    cursor.close()

