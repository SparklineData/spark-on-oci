# Connecting to JDBC over HTTP with `pyhive`

Copy the `thrift_http_transport.py` file to import it into your code.

You can run the file for testing:

```bash
python thrift_http_transport.py https://<ip>/jdbc <username> <password>`
```

Here is an example of how to use it in your Python code:

```python
from pyhive import hive
from thrift_http_transport import http_mode
import sys

url = sys.argv[1]
username = sys.argv[2]
pw = sys.argv[3]
conn = hive.connect(thrift_transport=http_mode(uri=url
                                               ,username=username
                                               ,password=pw
                                               ,verify=False))
cursor = conn.cursor()
cursor.execute("show databases")
print(cursor.fetchone())
cursor.execute("show databases")
print(cursor.fetchone())
cursor.close()
```
