FROM python:3.6-stretch
COPY . /tmp/spark-on-oci
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && \
    chmod a+x kubectl && \
    mv kubectl /usr/local/bin/kubectl && \
    wget -qO- 'https://get.helm.sh/helm-v2.14.1-linux-amd64.tar.gz' | tar xzv -C /usr/local/bin --strip-components=1 && \
    pip install /tmp/spark-on-oci oci-cli && \
    rm -rf /tmp/spark-on-oci
CMD ["/bin/bash"]
