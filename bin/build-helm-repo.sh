#!/bin/bash
set -e
set -u
# Run this in the linkyard/docker-helm Docker image

CHART_REPO_URL=$1
CHART_REPO_DIR=$2

helm init --client-only
echo \"User-Agent: *\nDisallow: /\" > ./public/charts/robots.txt
helm repo add telegraf-ds http://influx-charts.storage.googleapis.com
helm repo add influxdb http://influx-charts.storage.googleapis.com
helm dep update charts/spark/
helm package charts/* --destination .
helm repo index --url ${CHART_REPO_URL} .
cat index.yaml
mv index.yaml *.tgz ${CHART_REPO_DIR}
