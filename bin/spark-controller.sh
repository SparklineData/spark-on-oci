#!/bin/bash
 
mkdir -p ~/.oci ~/.kube

docker run --rm -it \
	--volume $HOME/.oci:/home/spark-controller/.oci \
	--volume $HOME/.kube:/home/spark-controller/.kube \
	--publish 8888:8888 \
	--name spark-controller \
	spark-controller $@
