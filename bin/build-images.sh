#!/bin/sh

set -e
set -x

IMAGE_DIR=${1}
IMAGE_NAME=${2}
shift
shift

docker login -u ${STAGING_REGISTRY_USER} -p ${STAGING_REGISTRY_PW} ${STAGING_REGISTRY}
docker pull ${IMAGE_NAME} ||  true
docker build $@ --cache-from ${IMAGE_NAME} --tag ${IMAGE_NAME} ${IMAGE_DIR} && docker push ${IMAGE_NAME}
