#!/bin/bash
set -e
set -u
set -x

API_VERSION=spark-on-oci.org/v1alpha1

OPERATOR_NAME=spark-operator
OPERATOR_IMAGE=$1
OPERATOR_IMAGE_TAG=$2
CHART_DIR=$3


OPERATOR_DEPLOY_DIR="deploy-${OPERATOR_NAME}-${OPERATOR_IMAGE_TAG}"
OPERATOR_NAMESPACE=spark

CHART_VERSION=0.1.0
CHART_NAME=spark

######
# Download the operator-sdk to build the operator
#
OPERATOR_SDK_VERSION=v0.9.0
if [[ "$(uname)" == "Darwin" ]]; then
wget https://github.com/operator-framework/operator-sdk/releases/download/${OPERATOR_SDK_VERSION}/operator-sdk-${OPERATOR_SDK_VERSION}-x86_64-apple-darwin -O bin/operator-sdk
else
wget https://github.com/operator-framework/operator-sdk/releases/download/${OPERATOR_SDK_VERSION}/operator-sdk-${OPERATOR_SDK_VERSION}-x86_64-linux-gnu -O bin/operator-sdk
fi

chmod a+x bin/operator-sdk

######
# Build the operator image

./bin/operator-sdk new ${OPERATOR_NAME} --api-version=${API_VERSION} --kind=SparkCluster --type=helm
rm -r ${OPERATOR_NAME}/helm-charts/sparkcluster
cp -r ${CHART_DIR} ${OPERATOR_NAME}/helm-charts/sparkcluster
cd ${OPERATOR_NAME}
../bin/operator-sdk build ${OPERATOR_IMAGE}:${OPERATOR_IMAGE_TAG}
cd ..

#####
# Add deployment files
#
cp -r ${OPERATOR_NAME}/deploy ${OPERATOR_DEPLOY_DIR}
cat <<END > ${OPERATOR_DEPLOY_DIR}/cluster-admin-role.yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: spark-rbac-1
subjects:
  - kind: ServiceAccount	# Reference to ServiceAccount's metadata.name
    name: ${OPERATOR_NAME}
    namespace: ${OPERATOR_NAMESPACE} 	# Reference to ServiceAccount's metadata.namespace
  - kind: User
    name: $(cat ~/.oci/config | grep user= | sed -e 's/user=//')
roleRef:
  kind: ClusterRole
  name: cluster-admin
  apiGroup: rbac.authorization.k8s.io
END

if [[ "$(uname)" == "Darwin" ]]; then
sed -i "" "s|REPLACE_IMAGE|${OPERATOR_IMAGE}:${OPERATOR_IMAGE_TAG}|g" ${OPERATOR_DEPLOY_DIR}/operator.yaml
else
sed -i "s|REPLACE_IMAGE|${OPERATOR_IMAGE}:${OPERATOR_IMAGE_TAG}|g" ${OPERATOR_DEPLOY_DIR}/operator.yaml
fi

####################
# example yaml for creating a spark cluster

NOTEBOOK_SECRET=$(base64 /dev/urandom | tr dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)

cat <<END > ${OPERATOR_DEPLOY_DIR}/spark-cluster.yaml
apiVersion: ${API_VERSION}
kind: SparkCluster
metadata:
  name: spark
spec:
  Notebook:
    Token: ${NOTEBOOK_SECRET}
  monitoring:
    enabled: false

  SparkApplication:
    Home: /app
    ImagePullPolicy: Always
    Env:
      SPARK_PROPERTIES: /conf/spark.properties
    Conf:
      spark.properties: "spark.stuff=nothing"
END

echo "#####################################################################"
echo  Created k8s Operator from Helm Chart ${CHART_NAME}-{CHART_VERSION}
echo  1. Push the Docker image: docker push ${OPERATOR_IMAGE}:${OPERATOR_IMAGE_TAG}
echo  2. Use kubectl to deploy the files in ${OPERATOR_DEPLOY_DIR}
echo    kubectl apply --namespace spark -f ${OPERATOR_DEPLOY_DIR}/crds/*crd.yml
echo    kubectl apply --namespace spark -f ${OPERATOR_DEPLOY_DIR}/service_account.yaml
echo    kubectl apply --namespace spark -f ${OPERATOR_DEPLOY_DIR}/cluster-admin-role.yaml
echo    kubectl apply --namespace spark -f ${OPERATOR_DEPLOY_DIR}/role.yaml
echo    kubectl apply --namespace spark -f ${OPERATOR_DEPLOY_DIR}/role_binding.yaml
echo    kubectl apply --namespace spark -f ${OPERATOR_DEPLOY_DIR}/operator.yaml
echo    kubectl apply --namespace spark -f ${OPERATOR_DEPLOY_DIR}/spark-cluster.yaml
echo "#####################################################################"
