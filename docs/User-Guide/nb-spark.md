# Using Notebooks with Spark

## Using external libraries.

The most common Python libraries for accessing Spark are:

* Dropbox's [PyHive](https://github.com/dropbox/PyHive) module.
* Impala [Impyla](https://github.com/cloudera/impyla) module.

This project primarily uses the PyHive driver.  See the links above
for detailed documentation.

## Connecting to Spark Thriftserver.

The Spark Thriftserver Host and Port information can be accessed
using the ```SparkController.print_cluster_endpoints``` API.  This method will
print all of the externally accessible endpoints:

```bash
http://129.146.104.185:32243 (spark-e4c7ildex028tlvakpd9o3kky-master-ui) 

http://129.146.136.161:30486 (spark-e4c7ildex028tlvakpd9o3kky-notebook) 

http://129.146.51.193:32062 (spark-e4c7ildex028tlvakpd9o3kky-thriftserver) 

http://129.146.51.193:31200 (spark-e4c7ildex028tlvakpd9o3kky-thriftserver-ui) 
```

Use the hostname and port provided by the `spark-<random-id>-thriftserver` in the connection string
to connect to the Spark Thriftserver.

## Spark Thriftserver Authentication.

Currently, by default no authentication or encryption is configured for the Spark Thriftserver.  This is because
the Thriftserver is not accessible from outside of the OKE cluster.  Only the Jupyter Notebook can access the Thriftserver
in the default configuration.

## Exposing Thriftserver's JDBC port.

Sometimes you will want to use external tools like OAC or Tableau to interact with Spark Thriftserver.

You can make the Thriftserver's JDBC port accessible from the internet by providing the `enable_jdbc=True` parameter in the
`SparkController.install_spark` command.

!!! danger """Your data is exposed"""

    If you expose the JDBC port, all traffic is visible to any nosy third party.
    
    Authentication and encryption will be added in later versions of this documentation.