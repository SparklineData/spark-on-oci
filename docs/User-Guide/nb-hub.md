# Enabling multiple users with JupyterHub.

Sometimes you want to let multiple members of your team have
access to Notebooks and Spark.  This can be accomplished by
using the [JupyterHub](https://jupyterhub.readthedocs.io/en/stable/)
project.

JupyterHub will automatically create new Jupyter Notebook servers for each user on your team.  It is integrated
with Kubernetes directly, allowing you to reuse your Kubernetes knowledge and tools for administration.

This diagram demonstrates the JupyterHub on Kubernetes architecture.

![JupyterHub Diagram](https://z2jh.jupyter.org/en/latest/_images/architecture.png)


## Starting JupyterHub

Details for using SparkController to start JupyterHub for you will be provided in future versions of this documentation.

For now, the external documentation for setting up JupyterHub can be found in the
[JupyterHub on Kubernetes](https://z2jh.jupyter.org/en/latest/) project.

