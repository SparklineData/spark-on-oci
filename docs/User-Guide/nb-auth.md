# Notebook Authentication

The notebook server, by default, uses the basic authentication provided by Jupyter.

## Randomly generated secret

By default, the notebook server will randomly generate a secret for you.  This means that you will
have to access the notebook server's log's using the `kubectl` tool to see the secret.

To get the automatically generated notebook secret, first make sure that `kubectl` is configured and installed. Then:

```bash
# Get the name of the notebook pod
$ kubectl get pods -n sparkns
NAME                                                            READY     STATUS    RESTARTS   AGE
spark-e4c7ildex028tlvakpd9o3kky-master-5cbd98f767-wkcdc         1/1       Running   0          47d
spark-e4c7ildex028tlvakpd9o3kky-notebook-6fb68b587-xtvmx        1/1       Running   0          47d
spark-e4c7ildex028tlvakpd9o3kky-thriftserver-7b9948c7d6-5d47t   2/2       Running   0          41d
spark-e4c7ildex028tlvakpd9o3kky-worker-0                        1/1       Running   0          47d
spark-e4c7ildex028tlvakpd9o3kky-worker-1                        1/1       Running   0          41d
spark-e4c7ildex028tlvakpd9o3kky-worker-2                        1/1       Running   0          47d
spark-e4c7ildex028tlvakpd9o3kky-worker-3                        1/1       Running   0          41d
spark-e4c7ildex028tlvakpd9o3kky-worker-4                        1/1       Running   0          47d
spark-e4c7ildex028tlvakpd9o3kky-worker-5                        1/1       Running   0          47d
spark-operator-6596b899f5-gxjxx 
```

And now take a look at the logs of the notebook server:

```bash
$ kubectl logs -n sparkns spark-e4c7ildex028tlvakpd9o3kky-notebook-6fb68b587-xtvmx
Copying contents of notebook home from /tmp/notebook_home to /home/spark-notebook
--- starting jupyter ---
Starting Jupyter Notebook
jupyter notebook  --notebook-dir /home/spark-notebook --ip=0.0.0.0 --port=8888 --NotebookApp.default_url=/tree
------------------------
[I 20:16:50.529 NotebookApp] Writing notebook server cookie secret to /home/spark-notebook/.local/share/jupyter/runtime/notebook_cookie_secret
[I 20:16:50.851 NotebookApp] [jupyter_nbextensions_configurator] enabled 0.4.1
[I 20:16:50.917 NotebookApp] Jupyter-Spark enabled!
[I 20:16:50.948 NotebookApp] JupyterLab extension loaded from /opt/conda/lib/python3.6/site-packages/jupyterlab
[I 20:16:50.948 NotebookApp] JupyterLab application directory is /opt/conda/share/jupyter/lab
[I 20:16:50.954 NotebookApp] nteract extension loaded from /opt/conda/lib/python3.6/site-packages/nteract_on_jupyter
[I 20:16:50.956 NotebookApp] Serving contents
[I 20:16:50.956 NotebookApp] The Jupyter Notebook is running at:
[I 20:16:50.956 NotebookApp] http://(spark-e4c7ildex028tlvakpd9o3kky-notebook-6fb68b587-xtvmx or 127.0.0.1):8888/?token=4fcf72dc262baf7b1e2ba754d6515c6ec7f3e28bc2a40518
[I 20:16:50.956 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[W 20:16:50.960 NotebookApp] No web browser found: could not locate runnable browser.
[C 20:16:50.960 NotebookApp] 
    
    To access the notebook, open this file in a browser:
        file:///home/spark-notebook/.local/share/jupyter/runtime/nbserver-11-open.html
    Or copy and paste one of these URLs:
        http://(spark-e4c7ildex028tlvakpd9o3kky-notebook-6fb68b587-xtvmx or 127.0.0.1):8888/?token=4fcf72dc262baf7b1e2ba754d6515c6ec7f3e28bc2a40518

```

Notice the the long number at the end of the URL?

    4fcf72dc262baf7b1e2ba754d6515c6ec7f3e28bc2a40518
 
This is the secret that you must enter into the Jupyter Notebook.

Pretty random right?

## Manually generated secret

!!! info "Manually generating secrets is unsafe"

    Humans are not good at generating passwords.  
    
    Thus, this option is not
    documented and can be enabled only by advanced users who are willing to read the code.
    
    We recommend using the "Randomly generated secret, because it requires users to have access to the Kubernetes cluster via `kubectl`
    


# Security Details

!!! danger "Regarding Security"

    Your Jupiter notebook server is **accessible by the entire internet** (to make is easy for you to use durring demonstrations).
    
    By default, SSL/HTTPS is not enabled.  Expect that every command you send is visible to 3rd parties.
    
    This is why it is highly advised that you **set a strong secret** when deploying the notebook so that unwanted guests cannot
    execute arbitrary code in your OCI tenancy.
    
    Security is a deep and complicated topic that is not currently covered fully in this project.  Please check with your
    friendly security specialist before using the tools provided in this repo.