# Persisting Notebooks in Object Storage

### No default persistence is provided

By default, your Jupyter Notebook are not persisted.  This means that if you delete the cluster
or the Jupyter Notebook pod is restarted, you will lose your notebooks.  This can be avoided by
manually copying the notebooks to your workstation often.

## Use OCI Object Storage for persistence.

You can use OCI Object Storage to automatically persist your Jupyter Notebooks.

This means that auto-saving and snapshoting will occure automatically just as
if you are running the notebooks locally.  But instead of notebooks being stored on your
local filesystem, they will be stored in OCI Object Store buckets.

## How it works.

This project uses a custom Jupyter Notebook "Content Manager" backend that
uses a S3 compatible interface to persist notebooks.  The SparkController
will automatically create the necessary credentials needed by the Jupyter Notebook server
to connect to OCI Object Storage.

Some considerations when choosing to use Object Storage:

1. Anyone with access to the bucket can access your notebooks.
2. Only a single notebook server can access a bucket at a time.  
3. If you create multiple Spark clusters, only the latest one will be able to access the bucket.  All other clusters
will lose access to the bucket.

## Setup

To setup an OCI Bucket to be used for persisting notebooks:

1. Create a new bucket on OCI. You can use the `oci` command line tool or
the online web console. 

2. Add the following configuraiton options to the `config` dictionary passed
into the `SparkController.create_spark_cluster` command.

```bash
config['Notebook']['UserBucket']['Name'] = <name of your new bucket>
```

Now continue with the `create_spark_cluster` command as usual.