# The Spark Operator

The Spark Operator is the Kubernetes object that is responsible for
managing the lifetime of the Spark clusters created on Kubernetes.



!!! danger "Why another Spark Operator Implementation?"

    There exist multiple implementations of Kubernetes Spark Operator in the wild.
    You can find other implementations on GitHub.  An unofficial implementation is provided
    by [Googls's Spark-on-k8s](https://github.com/GoogleCloudPlatform/spark-on-k8s-operator) project.
    
    This project provides its own implementation of a Spark Operator that supports:
    
    * compatibility with any version of Spark.
    * compatibility with the Kubernetes-Spark scheduler (when using Spark 2.3.0 or higher).
    * benchmarking performance of different schedulers.
    * JDBC access via Spark's Hive Thriftserver.
    * configurable monitoring and logging
    
## Implementation details

The Spark Operator is a **[Kubernetes Operator](https://coreos.com/operators/)** implemented using the **[Helm Operator SDK](https://github.com/operator-framework/operator-sdk/blob/master/doc/helm/user-guide.md)**
as a wrapper around the **[Helm Charts](https://helm.sh/docs/developing_charts/)** provided in [this repository](https://gitlab.com/SparklineData/spark-on-oci/tree/master/charts/spark).

## Installing the Spark Operator.

The Spark Operator can be installed using the `SparkController` module in this project or manually
using Helm Charts provided in the `charts` directory.

### Using the Spark Controller.

The Spark Operator can be installed automatically using the [`Spark Controller`](Reference/spark_controller/) API.

The `SparkController.install_spark` method will deploy the Spark Operator on the Kubernetes cluster if it does not already exist.  All interactions
with the Spark Operator are transparently managed by the Spark Controller.

### Using Helm


The Helm Charts can be used directly using the [`helm` client](https://helm.sh/docs/using_helm/).  

Using the `helm` charts directly gives you the ability to manually configure all aspects of the Spark-on-OCI deployment.  

This repo provides all the tools needed to build and publish your customized Spark Helm Charts.