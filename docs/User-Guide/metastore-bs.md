# Hive Metastore on Block Storage Volumes.

It is possible to use [OCI Block Storage](https://docs.cloud.oracle.com/iaas/Content/Block/Concepts/overview.htm) to
persist the Spark Hive Metastore.

!!! warning "Limited support for automatic provisionning"

    This project uses the OCI Volume Provisioner to automatically provision volumes for your Metastore.
    However, the current state of the OCI Volume provisioner does not provide full support
    for managing volumes.  The following actions must be performed manually:
    
    * deleting volume
    * resizing volume
    * reusing existing volume.
    
    Refer to the [OCI FlexVolume Documentation](https://github.com/oracle/oci-flexvolume-driver) for more details.

This project uses the [OCI Volume Provisioner](https://github.com/oracle/oci-volume-provisioner) to integrate Kubernetes with 
OCI Block Storage.

## Using the SparkController to manage Metastore Block Volumes

As mentioned above, OKE does not yet provide the full range of features needed to automate
the management of Block Volumes for Metastore.  Thus, we will document the usage of
SparkController to manage the metastore when the missing capabilities become available.

For now, you can use the [Metastore in Object Storage](User-Guide/metastore-os.md) documentation for
easily persisting the Metastore across cluster lifecycles.

# Manually using Block Volumes

If you want to manually use the Block Volumes, the following steps provide an idea of how
the Helm Chart configurations can be modified.

## Enable OCI Permissions

Please ensure that the credentials used in the secret have the following privileges in the OCI API by creating a policy tied to a group or user.

```
Allow group <name> to manage volumes in compartment <compartment>
Allow group <name> to manage file-systems in compartment <compartment>
```

## Create a PVC

Next we'll create a PersistentVolumeClaim (PVC).

The storageClassName must match the "oci" storage class supported by the provisioner.

The matchLabels should contain the (shortened) Availability Domain (AD) within which you want to provision the volume. For example in Phoenix that might be PHX-AD-1, in Ashburn US-ASHBURN-AD-1, in Frankfurt EU-FRANKFURT-1-AD-1, and in London UK-LONDON-1-AD-1.

```yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: thriftserver-volume
spec:
  storageClassName: "oci"
  selector:
    matchLabels:
      failure-domain.beta.kubernetes.io/zone: "PHX-AD-1"
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 50Gi
```

### Create a block volume from a backup

You can use annotations to create a volume from an existing backup. Simply use an annotation and reference the volume OCID.

```yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: thriftserver-volume-from-backup
  annotations:
    volume.beta.kubernetes.io/oci-volume-source: ocid...
spec:
  storageClassName: "oci"
  selector:
    matchLabels:
      failure-domain.beta.kubernetes.io/zone: "PHX-AD-1"
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 50Gi
```