# Using the Kubernetes CLI Tool

The Kubernetes CLI (called `kubectl`) provides access to the Kubernetes resources.
It is the easiest way to get low-level access to the features provided by Kubernetes.

