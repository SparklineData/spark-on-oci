# Using an External Metastore.

You can configure the Spark Thriftserver to use an external
database (like MySQL) as the Metastore.  This is useful for advanced
use cases where the metadata resides outside of your Spark Cluster.

Configuring and external metastore requires modifying the `hive-config.xml`

**TODO** Provide details on editing the Helm Chart

### References:

1. https://mapr.com/docs/60/Hive/Config-MySQLForHiveMetastore.html
2. https://www.cloudera.com/documentation/enterprise/5-6-x/topics/cdh_ig_hive_metastore_configure.html