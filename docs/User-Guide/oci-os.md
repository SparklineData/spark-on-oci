# Accessing data with OCI Object Storage

You can use Instance Principles to access Object Storage without generating and copying credentials around.

For more details, see the documentation for the [HDFS Connector](https://docs.cloud.oracle.com/iaas/Content/API/SDKDocs/hdfsconnector.htm)

Also see [this blog](https://blogs.oracle.com/cloud-infrastructure/announcing-instance-principals-for-identity-and-access-management) for how to set up Instance Principle