# Enabling NVMe for Fast Disk IO

Oracle Cloud Infrastructure also provides instances
with locally attache NVMe devices.  These instances have excellent disk IO performance
which can improve query times.