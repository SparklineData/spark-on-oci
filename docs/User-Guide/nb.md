# Jupyter Notebooks

Jupyter Notebooks provide an interactive and collaborative interface
to Spark's capabilities.  

The Jupyter Notebook server should already have most of Python's [useful tools for analyzing data](https://github.com/onurakpolat/awesome-bigdata) already installed.

## Installing the Jupyter Notebook.

The Jupyter Notebook is installed automatically as a part of the
`SparkController.install_spark` command.

To have more control over the Jupyter Notebook deployment, you can use
the Helm Chart provided in [this repo](https://gitlab.com/SparklineData/spark-on-oci/blob/master/charts/spark/templates/notebook-deployment.yaml)

Most of the common configuration properties are already provided to you through the
Spark Operator.