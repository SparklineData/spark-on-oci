# Hive Metastore on Object Store Buckets.

This project contains the tools need to backup and restore Spark's Hive Metastore
in [OCI Object Storage](https://docs.cloud.oracle.com/iaas/Content/Object/Concepts/objectstorageoverview.htm) buckets.

### Advantages

* Lowest cost - You can save money by storing the Metastore in buckets instead of on Block Storage
* Easy to clone - multiple users can clone and reuse the Metastore as independant copies.
* Transparently managed - Spark Controller will automatically backup and restore the Metastore
upon creation, deletion, and restarts.
* Easy to switch between different Metastore metadata.

### Limitations

* Object Storage supports **eventual consistency**, meaning the latest backup may not be visible to all users.  However, this
just requires the users to wait and try again.

# How it works.

The loading and saving of the metadata in the Metastore is managed automatically by Kubernetes.

You can inspect the Helm Chart for the Spark Thriftserver to see details on how this is done.

The Helm Charts use [Kubernetes Lifecycle Hooks](https://kubernetes.io/docs/concepts/containers/container-lifecycle-hooks/) to implement
the save and load functionality.

The save and load operations can reference different objects. This makes it easy to make save work without overwriting existing metadata

# Setup and Configuration.

To save the Metastore, add the following fields to the `config` dictionary passed into the `SparkController.install_spark` command.

```python
config['saveMetastore'] = {
    'bucket': '<name of bucket to use>',
    'enabled': True,
    'object': 'metastore_db.tar.gz' # object name to save to
    }
}
```

To load the Metastore, add the following.


```python
config['loadMetastore'] = {
    'bucket': '<name of bucket to use>',
    'enabled': True,
    'object': 'metastore_db.tar.gz' # object name to load from
    }
}
```