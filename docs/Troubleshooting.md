# Troubleshooting


### Problem: I get strange errors when trying to access data in Object Storage.

The HDFS connector sometime provides an unhelpful error message:

```
java.io.IOException: Unable to determine if path is a directory
```

#### Solution

Take a look at the Thriftserver logs. You may not have the appropriate permissions for that object or bucket.  

Also ensure that Instance Principles are properly setup.  The the section on [Object Storage](/User-Guide/oci-os.md)