# Deploying the SparkCluster Using `kubectl`

You can deploy SparkClusters using the `kubectl` tool.

!!! hint "Setup (just once)"
  
    ```bash
    kubectl apply --namespace spark -f deploy/custom-resource.yml
    ```

    ```bash
    kubectl apply --namespace spark -f deploy/service-account.yaml
    ```

    ```bash
    kubectl apply --namespace spark -f deploy/cluster-admin-role.yaml
    ```

    ```bash
    kubectl apply --namespace spark -f deploy/role.yaml
    ```
 
    ```bash
    kubectl apply --namespace spark -f deploy/operator.yaml
    ```

!!! hint "Bring up the Spark nodes"
  
    ```bash
    kubectl apply --namespace spark -f deploy/spark-cluster.yaml
    ``` 

!!! hint "List existing `SparkCluster`s"

    ```bash
    kubectl get sparkclusters --namespace spark
    ```

!!! hint "List the running containers"

    ```bash
    kubectl get pods -n spark
    ```

    ```
    NAME                                                        READY     STATUS    RESTARTS   AGE
    spark-9atdorg2k6jerfboho3agtn69-master-6cd9b8fd57-czwqj     1/1       Running   0          21h
    spark-9atdorg2k6jerfboho3agtn69-notebook-667497bff9-fk4vb   1/1       Running   0          1h
    spark-9atdorg2k6jerfboho3agtn69-thriftserver-0              2/2       Running   0          21h
    spark-9atdorg2k6jerfboho3agtn69-worker-0                    1/1       Running   0          21h
    spark-9atdorg2k6jerfboho3agtn69-worker-1                    1/1       Running   0          21h
    spark-9atdorg2k6jerfboho3agtn69-worker-2                    1/1       Running   0          21h
    spark-operator-6596b899f5-5gvk8                             1/1       Running   0          21h
    ```

-----

# Kubernetes Manifest Files

## `spark_on_oci/deploy/custom-resource.yaml`
{{code_from_file("../spark_on_oci/deploy/custom-resource.yaml", "yaml")}}

## `spark_on_oci/deploy/service-account.yaml`
{{code_from_file("../spark_on_oci/deploy/service-account.yaml", "yaml")}}

## `spark_on_oci/deploy/cluster-admin-role.yaml`

!!! warning "Dont forget to set the `subjects:name` to the user's OCID."

{{code_from_file("../spark_on_oci/deploy/cluster-admin-role.yaml", "yaml")}}

## `spark_on_oci/deploy/role.yaml`
{{code_from_file("../spark_on_oci/deploy/role.yaml", "yaml")}}

## `spark_on_oci/deploy/operator.yaml`
{{code_from_file("../spark_on_oci/deploy/operator.yaml", "yaml")}}

## `spark_on_oci/deploy/spark-cluster.yaml`

!!! warning "Dont forget to set the `Spec:Notebook:Token` variable to a secret password."

{{code_from_file("../spark_on_oci/deploy/spark-cluster.yaml", "yaml")}}
