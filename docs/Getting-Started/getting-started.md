# Getting Started

The steps for setting up Spark on OCI

0. [Follow the Steps for Preparing for Kubernetes](https://docs.cloud.oracle.com/iaas/Content/ContEng/Concepts/contengprerequisites.htm)
1. [Create the OCI Configuration File](Creating-the-OCI-config-file/create-oci-config.md)
2. [Installing Docker](installing-docker.md)
3. [Running the Spark-on-OCI Notebook](starting-spark-controller-notebook.md)
