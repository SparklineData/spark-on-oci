# Running the Spark Controller Notebook

The following command will run the **Spark Controller Notebook** in a Docker container.

### 1. Create the Dockerfile for the Spark Controller image

Create a file called `Dockerfile` with the following contents

{{code_from_file("../Dockerfile","dockerfile")}}

### Build the Docker Image

```bash
HOST=0.0.0.0
# or HOST=$(dig +short myip.opendns.com @resolver1.opendns.com)

docker build  --build-arg NB_HOST_ARG="${HOST}" \
        --build-arg NB_UID_ARG=$(id -u $USER) \
        --build-arg http_proxy --build-arg HTTPS_PROXY \
        -t spark-controller .
```

### Run the Spark Controller Container

{{code_from_file("../bin/spark-controller.sh","bash")}}

!!! hint "Now check the link in the logs to fetch the latest notebook!"

    ```bash
    docker logs spark-controller
    ```
