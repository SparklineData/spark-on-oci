# Generate and Upload key files

We need to generate both a **public** and **private** key and upload the *public* key to the Console


??? question "Why do we need these keys?"
    Your API requests will be signed with your **private** key, and Oracle will use the **public** key to verify the authenticity of the request.

    You must **only upload** the **public** key to IAM (instructions below).

## Generating the keys

1. If you haven't already, create a `.oci` directory to store the credentials:

    ```bash
    mkdir -p ~/.oci
    ```

2. Generate the **private** key with one of the following commands.

    !!! hint "Recommended: With Passphrase"

        To generate the key, encrypted with a passphrase you provide when prompted:

        ``` tab='With Passphrase'
        openssl genrsa -out ~/.oci/oci_api_key.pem -aes128 2048

        ```

        ``` tab='No Passphrase'
        openssl genrsa -out ~/.oci/oci_api_key.pem 2048
        ```

        If you use a passphrase, you will alo need to set the `pass_phrase` field on the config file:

        !!! example "~/.oci/config"

            ```ini
            pass_phrase=examplephrase
            ```

    !!! danger "Keep it private! This key should never leave this computer"

    You can now set the `key_file` field in the config file

    !!! success "~/.oci/config"

        ```ini
        key_file=~/.oci/oci_api_key.pem
        ```

3. Ensure that only you can read the private key file:

    ```bash
    chmod go-rwx ~/.oci/oci_api_key.pem
    ```

4. Generate the **public** key:

    ```bash
    openssl rsa -pubout -in ~/.oci/oci_api_key.pem -out ~/.oci/oci_api_key_public.pem
    ```

5. Generate the **private** key's fingerprint

    ```bash
    openssl rsa -pubout -outform DER -in ~/.oci/oci_api_key.pem | openssl md5 -c
    ```

    You can now set the `fingerprint` field in the config file

    !!! success "~/.oci/config"

        ```ini
        fingerprint=20:3b:97:13:55:1c:5b:0d:d3:37:d8:50:4e:c5:3a:34
        ```


## Upload the **public** Key

You must upload the public PEM key in the Console, located at https://console.us-phoenix-1.oraclecloud.com.

1. Open the Console, and sign in.
2. View the details for the user who will be calling the API with the key pair:

    * If you're signed in as the user: Open the **User** menu (![User menu icon](https://docs.cloud.oracle.com/iaas/Content/Resources/Images/usermenu.png)) and click **User Settings**.
    * If you're an administrator doing this for another user, instead click **Identity**, click **Users**, and then select the user from the list.

3. Click **Add Public Key**
4. Paste the contents of the **public** PEM key (`~/.oci/oci_api_key_public.pem`) in the dialog box and click **Add**.

    The key's fingerprint is displayed (for example, `12:34:56:78:90:ab:cd:ef:12:34:56:78:90:ab:cd:ef`).

??? hint "You can have multiple keys per user"
    After you've uploaded your first public key, you can also use the [UploadApiKey](https://docs.cloud.oracle.com/api/#/en/identity/latest/ApiKey/UploadApiKey)
     API operation to upload additional keys.  You can have up to three API key pairs per user.

     In an API request, you specify the key's fingerprint to indicate which key you're using to sign the request.