# Select a Region

The `region` field represents Oracle Cloud Infrastructure region.

!!! info "See more info at [Regions and Availability Domains](https://docs.cloud.oracle.com/iaas/Content/General/Concepts/regions.htm)."

To see the available regions
for your tenancy, go to the Console at https://console.us-phoenix-1.oraclecloud.com/a/tenancy/regions

1. Go to the Oracle Cloud Infrastructure Console and click on **Administration**, click on **Tenancy Details**
2. Select the desired region
    * To choose the home region, in the **Tenancy Information** section, copy the **Home Region**
    * To choose a different region, go the **Regions** section an copy the **Name** of the desired region

You can now set the `region` field in the config file

!!! success "~/.oci/config"

    ```ini
    region=us-phoenix-1
    ```