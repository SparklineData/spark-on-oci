# What is the OCI Config File

To interact with OCI components, we need to first create
a configuration file that contains details of the OCI tenancy and credentials.

!!! tip "See the docs"
    For more details on configuration, see the [OCI Documentation](https://docs.cloud.oracle.com/iaas/Content/API/Concepts/sdkconfig.htm)

Create the config file into the default location `~/.oci/config`

The file should look something like this:

!!! example "~/oci/config"

    ```ini
    [DEFAULT]
    user=ocid1.user.oc1..aaaaaaaat5nvwcna5j6aqzjcaty5eqbb6qt2jvpkanghtgdaqedqw3rynjq
    fingerprint=20:3b:97:13:55:1c:5b:0d:d3:37:d8:50:4e:c5:3a:34
    key_file=~/.oci/oci_api_key.pem
    tenancy=ocid1.tenancy.oc1..aaaaaaaaba3pv6wkcr4jqae5f15p2b2m2yt2j6rx32uzr4h25vqstifsfdsq
    region=us-ashburn-1
    ```
