
# Get the User and Tenancy OCIDs

We need to get the `user` and `tenancy` fields.  Both fields are OCIDs, and are found in the Console, which is located at https://console.us-phoenix-1.oraclecloud.com.

!!! hint "Getting Access"

    If you don't have a login and password for the Console, contact an administrator. If you're not familiar with OCIDs, see [Resource Identifiers](https://docs.cloud.oracle.com/iaas/Content/General/Concepts/identifiers.htm).

## Tenancy's OCID

Get the tenancy OCID from the Oracle Cloud Infrastructure Console on the Tenancy Details page:

1. Open the navigation menu, under Governance and Administration, go to Administration and click Tenancy Details.

    ![Navigation menu showing Tenancy Details item](https://docs.cloud.oracle.com/iaas/Content/Resources/Images/tenancydetailsmenu_thumb_400_0.png)

2. The tenancy OCID is shown under Tenancy Information. Click Copy to copy it to your clipboard.

    ![Tenancy Details page showing the location of the tenancy OCID](https://docs.cloud.oracle.com/iaas/Content/Resources/Images/tenancydetailspage_thumb_400_0.png)

You can now set the `tenancy` field in the config file.


!!! success "~/.oci/config"


    ```ini
    tenancy=ocid1.tenancy.oc1..aaaaaaaaba3pv6wkcr4jqae5f15p2b2m2yt2j6rx32uzr4h25vqstifsfdsq
    ```

## User's OCID

Get the user's OCID from the Console on the page showing the user's details. To get to that page:

* If you're signed in as the user: Open the **User** menu (![User menu icon](https://docs.cloud.oracle.com/iaas/Content/Resources/Images/usermenu.png)) and click **User Settings**.
* If you're an administrator doing this for another user: Open the navigation menu. Under **Governance and Administration**, go to **Identity** and click **Users**. Select the user from the list.

The user OCID is shown under **User Information**. Click Copy to copy it to your clipboard.

You can now set the `user` field in the config file


!!! success "~/.oci/config"

    ```ini
    user=ocid1.user.oc1..aaaaaaaat5nvwcna5j6aqzjcaty5eqbb6qt2jvpkanghtgdaqedqw3rynjq
    ```
    
## User's Permissions

The OCI User should have sufficient permissions for accessing all the necessary resources.

This documentation assumes that the user is in the **Administrators** Group

!!! hint "More Info"

    For more details see the [Policy Configuration](https://docs.cloud.oracle.com/iaas/Content/ContEng/Concepts/contengpolicyconfig.htm#PolicyPrerequisitesService) documentation.