# Running Spark on OCI

This project provides a tutorial and tools for running Apache Spark 
on Oracle Cloud Infrastructure (OCI)

1. [Getting Started](Getting-Started/getting-started.md) - Start from scratch
2. [SparkController Reference](Reference/spark_controller.md) - Documentation for the Python module

----

# Features

  * Runs Spark on Oracle Kubernetes Engine
  * Uses Oracle Object Storage to persist large datasets
  * Uses fast NVMe SSDs  attached storage
  * Can utilies VMs and bare metal