# Chart values

Here are the configuration values for the Helm chart

{{code_from_file("../charts/spark/values.yaml", "yaml")}}

